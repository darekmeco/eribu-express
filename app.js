"use strict";

global.__basedir = __dirname
global.__publicdir = __dirname + '/public';
global.__assetsdir = __dirname + '/public/assets';

var createError = require('http-errors');
var express = require('express');
var path = require('path');
const fileUpload = require('express-fileupload');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose')
const cors = require('cors')
const resizer = require('./libs/images/resizer');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var customersRouter = require('./modules/customer/routes');
var productsRouter = require('./modules/products/routes');
var mediaRouter = require('./modules/media/routes');
var app = express();

mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true})

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload());
app.use(cors());

app.use('/images', resizer);
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/customer', customersRouter);
app.use('/product', productsRouter);
app.use('/media', mediaRouter);

// catch 404 and forward to error handler
//
// app.use(function(req, res, next) {
//   next(createError(404));
// });
app.use((err, req, res, next) => {
  if (err.isServer) {
    // log the error...
    // probably you don't want to log unauthorized access
    // or do you?
  }

  console.log(err.output);
  return res.status(err.output.statusCode).json(err.output.payload);
});

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};
//
//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

module.exports = app;
