import * as express from 'express';
import mediaRouter from '../modules/media/routes';
import {resizerRouter} from '@eribu/images';
import indexRouter from './default';

export const register = (app: express.Application) => {
  app.use('/images', resizerRouter);
  app.use('/', indexRouter);
  app.use('/media', mediaRouter);
};
