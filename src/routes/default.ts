import express from 'express';
const router = express.Router();

/* GET home page. */
router.get('/', (req: express.Request, res: express.Response, next: express.NextFunction) => {
  global.logger.log({
    level: 'info',
    message: 'Hello distributed log files!',
  });
  return res.json({ title: 'Express' });

});

export default router;
