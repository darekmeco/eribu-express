declare namespace NodeJS {
  interface Global {
    baseDir: string;
    logger: any;
    assetsDir: string;
    modulesDir: string;
    router: EribuRouter;
    controllers: [];
  }
}
declare var global: NodeJS.Global;
