import { Schema, HookNextFunction, Model } from 'mongoose';
import Slug from 'slug';
import { EribuDocument } from './types';

export function Slugify(schema: Schema) {
  const fieldName = 'name';
  schema.methods.slugify = async function(slug: string, model: any) {
    const duplicate = await model.findOne({ slug }).where('_id').ne(this._id);
    // global.logger.info(`duplicate:, ${duplicate}`);
    if (duplicate) {
      const newslug = slug + '-';
      slug = await this.slugify(newslug, model);
    }
    return slug;
  };
  schema.add({
    slug: {
      type: String,
      slug: fieldName,
      unique: true,
      required: [true, 'Wymagane'],
    },
  });
  schema.pre('validate', async function(this: EribuDocument, next: HookNextFunction) {
    // global.logger.info(`pre this, ${this.constructor}`);
    const model = this.constructor;
    const source: string = this.get(fieldName);
    if (fieldName && source) {
      const slug = Slug(source, { lower: true });
      const final = await this.schema.methods.slugify(slug, model);
      // global.logger.info(`final slug: ${final}`);
      this.slug = final;
    }
  });

}
