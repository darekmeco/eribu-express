import { Schema } from 'mongoose';
export const HasStatus  = function HasStatus(schema: Schema) {
  schema.add({
    status: Number
  });
}
