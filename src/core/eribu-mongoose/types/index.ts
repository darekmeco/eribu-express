import { Document } from 'mongoose';
export interface EribuDocument extends Document {
    slug: string;
}
