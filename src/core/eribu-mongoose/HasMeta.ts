import { Schema } from 'mongoose';
export function HasMeta(schema: Schema) {
  schema.add({
    meta_title: String,
    meta_description: String,
    og_title: String,
    og_description: String,
    og_type: String
  });
}
