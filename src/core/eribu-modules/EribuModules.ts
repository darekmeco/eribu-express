import fs from 'fs';
import path from 'path';
import { EribuConfig } from '@eribu/modules';
import { File } from '@eribu/filesystem';
import { Application } from 'express';
import boom from '@hapi/boom';
export class EribuModules {

  private configFiles: File[] = [];
  private configs: EribuConfig[] = [];
  private controllers: [] = [];
  private app: Application;

  constructor(app: Application) {
    this.app = app;
    global.modulesDir = path.join(global.baseDir, '/modules');
  }
  /**
   * Initiate modules and routes
   * @return [Promise<void>]
   */
  public async init(): Promise<void> {
    global.controllers = [];
    await this.readConfigs();
    await this.registerRoutes().catch((res) => {
      console.log(res);
      global.logger.error(`${__filename}: ${res}`);
    });
  }
  /**
   * Get EribuConfig array array
   * @return [EribuConfig[]]
   */
  public getConfigs(): EribuConfig[] {
    return this.configs;
  }
  /**
   * Get File array
   * @return [Filr[]]
   */
  public getConfigFiles(): File[] {
    return this.configFiles;
  }

  private async registerControllers(routes: [EribuRoute], config: EribuConfig): Promise<void> {
    global.logger.info('EribuModules::registerControllers');

    if (config.configFile) {
      // const controllersPath: string = path.join(config.configFile.getDir(), '/controllers');
      const controllersFilePath: string = path.join(config.configFile.getDir(), 'controllers');
      global.logger.info(`controllersFilePath: ${controllersFilePath}`);
      // console.log('routes:', routes);
      
      routes.forEach(async (route: EribuRoute) => {

        const controllerFilePath = path.join(controllersFilePath, `${route.controller}.js`);
        /**
         * Controller object class
         */
        const controller = await import(controllerFilePath);

        if (global.controllers.findIndex((el: any) => {
          return el.name === route.controller;
        }) === -1) {
          console.log('pushing controller', route);

          global.controllers.push({ name: route.controller, controller: controller.default } as never);
          console.log('pushed controllers', global.controllers);
        }
      });
    }
  }

  /**
   * Register all routes from modules
   * @return [description]
   */
  private async registerRoutes(): Promise<void> {

    const activeConfigs = this.getActiveConfigs();

    for (const config of activeConfigs) {
      if (config.configFile) {

        const routePath = path.join(config.configFile.getDir(), '/routes');

        global.logger.info(`Try to load routes from, ${routePath}`);
        if (!fs.existsSync(routePath)) {
          throw (boom.badData(`Route directory not exists: ${routePath}`));
        }
        const routeFile = path.join(routePath, 'index.js');
        if (!fs.existsSync(routeFile)) {
          throw (boom.badData(`Route module file not exists: ${routeFile}`));
        }
        const route = await import(routePath);
        /**
         * Todo: delete old style router
         */
        if (route.default && typeof (route.default) === 'function') {
          global.logger.info('function', typeof (route.default) === 'function');
          this.app.use(config.uri, route.default);
        } else if (route.default && typeof route.default === 'object') {

          // TODO: Validate route.default
          await this.registerControllers(route.default, config);
          global.router.append(route.default);
          this.app.use(config.uri, global.router.getRouter()); // TODO: check if global.router.getRouter() not returns duplicates ????

        }
        console.log('route:::s', typeof route.default, typeof (route.default) === 'function', typeof (route.default) === 'object');
        // this.app.use(config.uri, route.default);
        global.logger.info(`Successfully loaded routes from, ${routePath}`);
      }
    }
  }
  /**
   * Get only active EribuConfig array
   * @return [description]
   */
  private getActiveConfigs() {
    return this.configs.filter((o: EribuConfig) => o.active === true);
  }
  /**
   * Read config object files for each module
   * @return [description]
   */
  private readConfigsFiles() {

    const files: string[] = fs.readdirSync(global.modulesDir);
    for (const file of files) {
      global.logger.info(`Try to register module: ${file}`);
      const dirFile: string = path.join(global.modulesDir, file);
      const dirent = fs.statSync(dirFile);
      if (dirent.isDirectory()) {

        const configFilePath: string = path.join(dirFile, '/module.js');

        const configFile: File = new File(configFilePath);

        if (configFile.hasReadAccess()) {

          this.configFiles.push(configFile);

        } else {
          global.logger.error(`Module ${file} has no readable config.`);
        }
      } else {
        global.logger.error(`Module ${file} is not directory`);
      }
    }
  }
  /**
   * Read EribuConfig objects for each module
   * @return [description]
   */
  private async readConfigs() {
    this.readConfigsFiles();

    for (const configFile of this.configFiles) {
      global.logger.info(`Reading ${configFile.getFilePath()} config file...`);
      const obj = await import(configFile.getFilePath());
      const config: EribuConfig = obj.Config;
      config.configFile = configFile;
      this.configs.push(config);
      global.logger.info(`OK`);
    }
  }
}
