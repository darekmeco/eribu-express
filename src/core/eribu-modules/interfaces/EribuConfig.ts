import { File } from '@eribu/filesystem';
export interface EribuConfig {
  name: string;
  uri: string;
  active: boolean;
  version: string;
  configFile?: File;
}
