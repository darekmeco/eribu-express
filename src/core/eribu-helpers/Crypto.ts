import crypto from 'crypto';
export interface PasswordData {
  salt: string;
  passwordHash: string;
}
export class Crypto {
  /**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
  public static genRandomString(length: number) {
    return crypto.randomBytes(Math.ceil(length / 2))
      .toString('hex') /** convert to hexadecimal format */
      .slice(0, length);   /** return required number of characters */
  };

  /**
   * hash password with sha512.
   * @function
   * @param {string} password - List of required fields.
   * @param {string} salt - Data to be validated.
   */
  public static sha512(password: string, salt: string) {
    const hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    const value = hash.digest('hex');
    return {
      salt: salt,
      passwordHash: value
    };
  };

  public static saltHashPassword(userpassword: string) {
    const salt = this.genRandomString(16); /** Gives us salt of length 16 */
    const passwordData: PasswordData = this.sha512(userpassword, salt);
    return passwordData;
  }

  public static validatePassword(password: string, salt: string, hash: string) {
    const passwordData = this.sha512(password, salt);
    if (passwordData.passwordHash === hash) {
      return true;
    }
    return false;
  }
}
