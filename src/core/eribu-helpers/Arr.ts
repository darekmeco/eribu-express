export class Arr {
  public static notEmpty(arr: any[]): boolean {
    return (Array.isArray(arr) && arr.length > 0);
  }
}
