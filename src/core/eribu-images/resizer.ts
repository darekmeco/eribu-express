import express from 'express';
import fs from 'fs';
import Jimp from 'jimp';
import path from 'path';

const router = express.Router();

class Resizer {

  public async index(req: express.Request, res: express.Response, next: express.NextFunction) {

    if (req.params.filename) {

      const requestFile = path.join(global.assetsDir, req.params.filename);
      const cacheFile = path.join(global.assetsDir, '/cache/', req.params.filename);

      console.log('req:', requestFile);
      if (fs.existsSync(requestFile) && !fs.existsSync(cacheFile)) {

        console.log('no cache');

        const image = await Jimp.read(requestFile);
        await image.cover(256, 256)
          .quality(80)
          .writeAsync(cacheFile);
      }
      if (fs.existsSync(cacheFile)) {
        return res.sendFile(cacheFile);
      }
    }
    return res.sendStatus(403);
  }

  public async thumb(req: express.Request, res: express.Response, next: express.NextFunction) {

    const {filename, width, height} = req.params;
    console.log(filename, width, height);
    if (filename && width && width) {

      const requestFile = path.join(global.assetsDir, req.params.filename);
      const cacheFile = path.join(global.assetsDir, '/cache/', `_${width}_${height}_`, req.params.filename);

      console.log('req:', requestFile);
      if (fs.existsSync(requestFile) && !fs.existsSync(cacheFile)) {

        console.log('no cache');

        const image = await Jimp.read(requestFile);
        await image.cover(parseInt(width, 10), parseInt(height, 10))
          .quality(80)
          .writeAsync(cacheFile);
      }
      if (fs.existsSync(cacheFile)) {
        return res.sendFile(cacheFile);
      }
    }
    return res.sendStatus(403);
  }
}

const resizer = new Resizer();
router.get('/thumb/:width/:height/:filename(*)', resizer.thumb);
router.get('/:filename(*)', resizer.index);
export const resizerRouter = router;
