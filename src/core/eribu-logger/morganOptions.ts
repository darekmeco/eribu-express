import { Options } from 'morgan';
export const morganOptions: Options = {
  stream: {
    write(message: string) {
      global.logger.info('Morgan stream: ', message.trim());
    },
  },
};
