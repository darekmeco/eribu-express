import winston from 'winston';
import { winstonOptions } from './winstonOptions';

class EribuLogger {
  private myFormat = winston.format.printf((info) => {
    return `  ${info.level}: ${info.timestamp} | ${info.message}`;
  });
  private logger: winston.Logger;

  constructor() {
    this.logger = winston.createLogger({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        this.myFormat,
      ),
      transports: [
        new winston.transports.File(winstonOptions.file),
        new winston.transports.Console(winstonOptions.console),
      ],
      exitOnError: false, // do not exit on handled exceptions
    });
  }
  public getLogger() {
    return this.logger;
  }
}
const eribuLogger = new EribuLogger();
export const logger = eribuLogger.getLogger();
