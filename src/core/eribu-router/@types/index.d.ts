declare interface EribuRouter {
  [key: string]: any;
  router: any;
  append(routes: [EribuRoute]): void;
}

declare interface EribuRoute {
  uri: string;
  as: string;
  controller: string;
  action: string;
  options: object;
  methods: [string];
}
