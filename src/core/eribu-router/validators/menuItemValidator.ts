import { body, ValidationChain } from 'express-validator/check';

export const menuItemValidator: ValidationChain[] = [
  body('name').not().isEmpty(),
  body('parentid').not().isEmpty(),
];
