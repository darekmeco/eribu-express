import { Router, NextFunction, Response, Request } from 'express';
import passport from 'passport';
import boom from '@hapi/boom';
import { ValidationChain, check, validationResult } from 'express-validator/check';
// import { menuItemValidator } from '../../modules/menu/validators/menuItemValidator';

interface Action {
  controller: any;
  method: string;
}

interface Options {
  validator?: ValidationChain[];
  middlewares?: [];
  secure: boolean;
}

/**
 * Eribu Main Router Class
 */
export class EribuRouter implements EribuRouter {
  [key: string]: any;
  public router: Router;
  private currentAction: Action;
  constructor() {
    this.router = Router();
  }
  public get(uri: string, action: Action, options: Options = { secure: false }) {
    this.currentAction = action;
    console.log('get route', action.controller, action.method);
    this.router.get(uri, this.middlewares(options), this.asyncMiddleware(action.controller[action.method]));
  }
  public post(uri: string, action: Action, options: Options = { secure: false }) {
    this.currentAction = action;
    this.router.post(uri, this.middlewares(options), this.asyncMiddleware(action.controller[action.method]));
  }
  public delete(uri: string, action: Action, options: Options = { secure: false }) {
    this.currentAction = action;
    this.router.delete(uri, this.middlewares(options), this.asyncMiddleware(action.controller[action.method]));
  }
  public getRouter() {
    return this.router;
  }
  /**
   * Append route to routes stack
   * @param  routes [description]
   * @return        [description]
   */
  public append(routes: [EribuRoute]) {
    global.logger.info('EribuRouter::append');
    routes.forEach((route: EribuRoute) => {
      console.log('global.controllers', global.controllers);
      console.log('appending route', route);

      // TODO: validate route object
      route.methods.forEach((method: string) => {
        const controller: any = global.controllers.find((el: any) => {
          return el.name === route.controller;
        });
        if (controller) {
          this[method](route.uri, { controller: controller.controller, method: route.action }, route.options);
        }
      });
    });
  }
  /**
   * Middlewares array for Router
   * Based on Options.
   * @param  options [description]
   * @return         [description]
   */
  private middlewares(options: Options): any[] {
    const middlewares: any[] = [];
    /**
     * Add jwt middleware if route is secured
     */
    if (options.secure) {
      middlewares.push(passport.authenticate('jwt', { session: false }));
    }
    /**
     * Add middlewares passed by route options
     */
    if (options.validator) {
      const newMiddlewares = middlewares.concat(options.validator);
      newMiddlewares.push(this.validatorErrorHandler);
      return newMiddlewares;
    }
    return [];
  }
  /**
   * Error Handler for Express Validator
   * @param  req  [description]
   * @param  res  [description]
   * @param  next [description]
   * @return      [description]
   */
  private validatorErrorHandler(req: Request, res: Response, next: NextFunction) {

    global.logger.info('Handling validator errors');
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      next(boom.notAcceptable('Not Valid', errors.array()));
    } else {
      next();
    }
  }

  private asyncMiddleware(fn: any) {
    return (req: any, res: any, next: any) => {
      if (fn) {
        Promise
          .resolve(fn(req, res, next))
          .catch((err) => {
            console.log(err);
            next(boom.badRequest(err));
          });
      } else {
        global.logger.error(`Router method not exists: ${fn}`);
        throw boom.notImplemented(`Router method not exists: ${fn}`);
      }
    };
  }
}
