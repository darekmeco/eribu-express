import { File } from './File';
import fs from 'fs';
import path from 'path';
import _isEmpty from 'lodash/isEmpty';
import _head from 'lodash/head';

export class FileSystem {
  public static readAllTree() {
    const startdir: string = global.assetsDir;
    const walkSync = (dir: string, filelist: any[] = []) => {
      const files = fs.readdirSync(dir);
      console.log('alll files', files);
      for (const file of files) {
        console.log('dir, file', dir, file);
        const dirFile = path.join(dir, file);
        try {
          const dirent = fs.statSync(dirFile);

          if (dirent.isDirectory()) {
            console.log('directory', path.join(dir, file));
            const odir = {
              file: dirFile,
              files: [] as any[],
            };
            odir.files = walkSync(dirFile, files);
            filelist.push(odir);
          } else {
            filelist.push({
              file: dirFile,
            });
          }

        } catch (err) {
          console.log('it does not exist');
          break;
        }

      }
      return filelist;
    };
    return walkSync(startdir);
  }
  /**
   * Reads assetsDir directory structure as array
   * @return [array]
   */
  public static readFoldersTree() {

    const startdir = global.assetsDir;
    const walkSync = (dir: string) => {
      const filelist: any[] = [];

      if (startdir === dir) {
        filelist.push({
          children: [] as any[],
          data: {
            filepath: dir,
            path: dir,
            path_relative: path.relative(global.assetsDir, dir),
          },
          title: '/',
          key: dir,
        });
      }

      console.log('startdir:', startdir);
      const files = fs.readdirSync(dir);
      for (const file of files) {
        const dirFile = path.join(dir, file);
        try {
          const dirent = fs.statSync(dirFile);
          if (dirent.isDirectory() && file !== 'cache') {
            console.log('directory', path.join(dir, file));
            const odir = {
              children: [] as any[],
              data: {
                filepath: dirFile,
                path: dir,
                path_relative: path.relative(global.assetsDir, dirFile),
              },
              title: file,
              key: path.relative(global.assetsDir, dirFile),
            };
            odir.children = walkSync(dirFile);

            if (_isEmpty(filelist) === false) {
              const head = _head(filelist);
              if (head) {
                head.children.push(odir);
              }
            } else {
              filelist.push(odir);
            }

          }
        } catch (err) {
          console.log(err);
        }
      }
      return filelist;
    };
    return walkSync(startdir);
  }

  public static readFilesWithPath(searchpath = global.assetsDir) {

    const dir = path.join(global.assetsDir, '/', searchpath);

    console.log('dir:', dir);

    const files = fs.readdirSync(dir);

    files.sort((a, b) => {
      return fs.statSync(dir + b).mtime.getTime() -
        fs.statSync(dir + a).mtime.getTime();
    });

    const fileList = [];
    for (const file of files) {
      const f = new File(path.join(dir, file));
      console.log('readFilesWithPath hasThumbnail: ', f.hasThumbnail());

      if (f.dirent && !f.dirent.isDirectory()) {
        fileList.push({
          filename: file,
          key: file,
          path: dir,
          searchpath,
        });
      }
    }
    return fileList;
  }

  public static deleteFileWithPath(searchpath: string, filename: string) {
    fs.unlinkSync(path.join(global.assetsDir, '/', searchpath, '/', filename));
  }
  public static checkDirectorySync(directory: string) {
    try {
      fs.statSync(directory);
    } catch (e) {
      fs.mkdirSync(directory, { recursive: true });
    }
  }
}
