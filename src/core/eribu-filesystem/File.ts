import fs, { Stats } from 'fs';
import path from 'path';

export class File {
  public dirent: Stats | null;
  private ext: string;
  private dir: string;
  private root: string;
  private base: string;
  private name: string;
  private filePath: string;

  constructor(filePath: string) {
    this.filePath = filePath;
    this.pathParse();
    this.dirent = this.exists() ? fs.statSync(filePath) : null;
  }
  public getDir() {
    return this.dir;
  }
  public getName() {
    return this.name;
  }
  public getFilePath() {
    return this.filePath;
  }
  public exists(): boolean {
    return fs.existsSync(this.filePath);
  }
  public hasReadAccess(): boolean {
    if (this.exists()) {
      try {
        fs.accessSync(this.filePath, fs.constants.R_OK);
        return true;
      } catch (err) {
        return false;
      }
    }
    return false;
  }
  public hasThumbnail(): boolean {
    const thumb = `${global.assetsDir}/cache/${this.name}${this.ext}`;
    try {
      fs.accessSync(thumb, fs.constants.R_OK);
      return true;
    } catch (err) {
      return false;
    }
  }

  private pathParse() {
    const pathParts = path.parse(this.filePath);
    this.dir = pathParts.dir;
    this.root = pathParts.root;
    this.base = pathParts.base;
    this.name = pathParts.name;
    this.ext = pathParts.ext;
  }

}
