import PageController from '../controllers/PageController';
import BlockController from '../controllers/BlockController';
import { EribuRouter } from '@eribu/router';
const pageController = new PageController();
const blockController = new BlockController();

const eribuRouter = new EribuRouter();
// page
eribuRouter.get('/pages/index', {controller: pageController, method: 'index'});
eribuRouter.post('/pages/store', {controller: pageController, method: 'store'});
eribuRouter.get('/pages/edit', {controller: pageController, method: 'edit'});
eribuRouter.post('/pages/update', {controller: pageController, method: 'update'});
eribuRouter.delete('/pages/delete', {controller: pageController, method: 'delete'});

// block
eribuRouter.get('/blocks/index', {controller: blockController, method: 'index'});
eribuRouter.post('/blocks/store', {controller: blockController, method: 'store'});
eribuRouter.get('/blocks/edit', {controller: blockController, method: 'edit'});
eribuRouter.post('/blocks/update', {controller: blockController, method: 'update'});
eribuRouter.delete('/blocks/delete', {controller: blockController, method: 'delete'});

export default eribuRouter.getRouter();
