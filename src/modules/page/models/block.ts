import mongoose, { Model, model, Document } from 'mongoose';
import { HasImages } from '../../media/mongoose/HasImages';
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: [true, 'Wymagane'],
  },
  slug: {
    type: String,
    required: [true, 'Wymagane'],
    unique: true,
  },
  content: {
    type: String,
    required: [true, 'Wymagane']
  },
  short_content: String,
}, {
    timestamps: true,
  });
schema.plugin(HasImages);
export const Block: Model<Document> = model<Document>('Block', schema);
