import mongoose, { Model, model, Document } from 'mongoose';
const Schema = mongoose.Schema;
import { HasImages } from '../../media/mongoose/HasImages';
import { HasMeta } from '@eribu/mongoose';

const schema = new Schema({
  name: {
    type: String,
    required: [true, 'Wymagane'],
  },
  slug: {
    type: String,
    required: [true, 'Wymagane'],
    unique: true,
  },
  content: {
    type: String,
    required: [true, 'Wymagane']
  },
}, {
    timestamps: true,
  });

schema.plugin(HasImages);
schema.plugin(HasMeta);
export const Page: Model<Document> = model<Document>('Page', schema);
