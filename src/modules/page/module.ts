import { EribuConfig } from '@eribu/modules';
export const Config: EribuConfig = {
  name: 'Pages',
  uri: '/page',
  active: true,
  version: '0.1',
};
