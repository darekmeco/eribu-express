import { EribuConfig } from '@eribu/modules';
export const Config: EribuConfig = {
  name: 'Blog',
  uri: '/blog',
  active: true,
  version: '0.1',
};
