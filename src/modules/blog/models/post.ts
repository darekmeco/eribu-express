import mongoose, { Model, model, Document } from 'mongoose';
const Schema = mongoose.Schema;
import { HasImages } from '../../media/mongoose/HasImages';
import { HasMeta } from '@eribu/mongoose';

const schema = new Schema({
  title: {
    type: String,
    required: [true, 'Wymagane'],
  },
  slug: {
    type: String,
    required: [true, 'Wymagane'],
    unique: true,
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'BlogCategory',
  },
  content: String,
}, {
    timestamps: true,
  });

schema.plugin(HasImages);
schema.plugin(HasMeta);
export const BlogPost: Model<Document> = model<Document>('BlogPost', schema);
