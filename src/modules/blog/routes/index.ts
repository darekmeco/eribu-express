import PostController from '../controllers/PostController';
import CategoryController from '../controllers/CategoryController';
import { EribuRouter } from '@eribu/router';
const postController = new PostController();
const categoryController = new CategoryController();

const eribuRouter = new EribuRouter();
// blogPost
eribuRouter.get('/posts/index', {controller: postController, method: 'index'});
eribuRouter.post('/posts/store', {controller: postController, method: 'store'});
eribuRouter.get('/posts/edit', {controller: postController, method: 'edit'});
eribuRouter.post('/posts/update', {controller: postController, method: 'update'});
eribuRouter.delete('/posts/delete', {controller: postController, method: 'delete'});

// blogCategory
eribuRouter.get('/categories/all', {controller: categoryController, method: 'all'});
eribuRouter.get('/categories/index', {controller: categoryController, method: 'index'});
eribuRouter.post('/categories/store', {controller: categoryController, method: 'store'});
eribuRouter.get('/categories/edit', {controller: categoryController, method: 'edit'});
eribuRouter.post('/categories/update', {controller: categoryController, method: 'update'});
eribuRouter.delete('/categories/delete', {controller: categoryController, method: 'delete'});

export default eribuRouter.getRouter();
