import express, { Request, Response, NextFunction } from 'express';
import { BlogCategory } from '../models/category';
import { Document } from 'mongoose';
import _chunk from 'lodash/chunk';
import boom from '@hapi/boom';
import _set from 'lodash/set';

class CategoryController {
    public async all(req: Request, res: Response) {
        const models = await BlogCategory.find().sort({ createdAt: 'desc' });
        res.json(models);
    }

    public async index(req: Request, res: Response) {
        console.log(req.query, 'req-query');
        let sorter = {};
        _set(sorter, req.query.field ? req.query.field : 'createdAt', req.query.orderBy=='ascend' ? 1 : -1);
        const models = await BlogCategory.find({
            $or: [
                {
                    'name': {
                        $regex: req.query.searchInput,
                        $options: 'ig'
                    }
                },
                {
                    'slug': {
                        $regex: req.query.searchInput,
                        $options: 'ig'
                    }
                }
            ]
        }).sort(sorter);
        const modelsForPage = _chunk(models, req.query.pageSize)[req.query.current - 1];
        res.json({
            results: modelsForPage,
            pagination: {
                total: models.length,
                page: req.query.current
            }
        });
    }

    public async store(req: express.Request, res: Response, next: NextFunction) {
        console.log('model', req.body);
        /**
         * Ensure we dont ovewrite an document id
         */
        Reflect.deleteProperty(req.body, '_id');
        const model = new BlogCategory(req.body);

        model.validate(async (error: any) => {
            console.log('error', error);
            if (error == null) {
                try {
                    const savedModel: Document = await model.save();
                    return res.json({
                        body: req.body,
                        model: savedModel,
                        params: req.params,
                        query: req.query,
                        status: 'success',
                    });
                } catch (err) {
                    return next(boom.badRequest(err));
                }

            }
            return res.json({
                body: req.body,
                error,
                params: req.params,
                query: req.query,
                status: 'error',
            });
        });
    }

    public async edit(req: express.Request, res: express.Response) {
        const model = await BlogCategory.findById(req.query.id);
        return res.json({
            status: 'success',
            model,
        });
    }

    /**
   * Update blogCategory document
   * @param  req [description]
   * @param  res [description]
   * @return     [description]
   */
    public async update(req: express.Request, res: Response, next: NextFunction) {

        const model: Document | null = await BlogCategory.findById(req.body._id);
        
        if (model) {
            model.validate(async (error: any) => {
                if (error == null) {
                    try {
                        await model.update({
                            $set: req.body,
                        });
                    } catch (err) {
                        return next(boom.badRequest(err));
                    }
                }
                return res.json({
                    model,
                    status: 'success',
                    test: 1,
                    error,
                    params: req.params,
                    body: req.body,
                    query: req.query,
                });
            });
        }
    }

    public async delete(req: express.Request, res: Response) {
        const model: Document | null = await BlogCategory.findById(req.query.id);
        if (model) {
            model.remove();
            return res.json({
                status: 'success',
                model,
            });
        }
    }

}

export default CategoryController;
