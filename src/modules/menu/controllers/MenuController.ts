import express, { Response, Request, NextFunction } from 'express';
import { MenuItem } from '../models/MenuItem';
import boom from '@hapi/boom';
import { Document } from 'mongoose';
import { NodeDocument } from '@eribu/nestedset-mongoose';
class MenuController {
  public async index(req: express.Request, res: Response) {
    const docs = await MenuItem.roots();
    res.json({
      results: docs,
      pagination: {
        total: docs.length,
        page: req.query.current ? req.query.current : 1,
      },
    });
  }
  /**
   * Create new root MenuItem
   * @param  req [Request]
   * @param  res [Response]
   * @return     [Response]
   */
  public async store(req: Request, res: Response, next: NextFunction) {
    const data = req.body.form;
    const doc = new MenuItem(data);
    try {
      if (!req.body.parent_id) {
        doc.saveRoot();
      } else {
        const parent: NodeDocument | null = await MenuItem.findById(req.body.parent_id);
        // console.log(parent, req.body.parent_id);
        if (parent) {
          await parent.append(doc);
        } else {
          return next(boom.badRequest('No parent in database'));
        }
      }
      return res.json({ status: 'success', doc });
    } catch (err) {
      next(boom.badRequest(err));
    }
  }
  public async tree(req: Request, res: Response, next: NextFunction) {
    console.log(req.params);
    const root: Document | null = await MenuItem.findById(req.params.menuid);
    if (root) {
      // await model.nestedArray();
    } else {
      // error
    }
  }
  /**
   * Return data for edit form
   * @param  req [description]
   * @param  res [description]
   * @return     [description]
   */
  public async edit(req: express.Request, res: express.Response) {
    global.logger.info(req.query.id);
    const document: NodeDocument | null = await MenuItem.findById(req.query.id);
    if (document) {
      return res.json({
        status: 'success',
        document,
        tree: await document.nestedArray(),
      });
    }
  }
  public async update(req: express.Request, res: Response, next: NextFunction) {
    const model: NodeDocument | null = await MenuItem.findById(req.body._id);
    if (model) {
      model.validate(async (error: any) => {
        console.log('error', error);
        if (error == null) {
          console.log('update model', model);
          console.log('up m req.body', req.body);
          try {
            await model.update({
              $set: req.body,
            });
          } catch (err) {
            return next(boom.badRequest(err));
          }
        }
        return res.json({
          model,
          status: 'success',
          test: 1,
          error,
          params: req.params,
          body: req.body,
          query: req.query,
        });
      });
    }
  }
}

export default new MenuController();
