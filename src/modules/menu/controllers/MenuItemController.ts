import { Response, Request, NextFunction } from 'express';
import { MenuItem } from '../models/MenuItem';
import { NodeDocument } from '@eribu/nestedset-mongoose';
import boom from '@hapi/boom';

class MenuItemController {
  /**
   * Show all MenuItem records
   * @param  req [Request]
   * @param  res [Response]
   * @return     [Response]
   */
  public async index(req: Request, res: Response) {
    const models = await MenuItem.find().sort({ createdAt: 'desc' });
    res.json(models);
  }
  /**
   * Create new MenuItem for menuid
   * @param  req [Request]
   * @param  res [Response]
   * @return     [Response]
   */
  public async append(req: Request, res: Response, next: NextFunction) {
    console.log(req.body, !req.body.parentid);
    const doc = new MenuItem(req.body);
    try {
      const parent: NodeDocument | null = await MenuItem.findById(req.body.parentid);
      // console.log(parent, req.body.parent_id);
      if (parent) {
        await parent.append(doc);
      } else {
        return next(boom.badRequest('No parent in database'));
      }
      const root = await doc.root();
      const nestedArray = await root.nestedArray();
      if (root) {
        return res.json({ status: 'success', doc, tree: nestedArray });
      } else {
        return next(boom.badRequest('Document has no root parent'));
      }
    } catch (err) {
      next(boom.badRequest(err));
    }
  }
  /**
   * Delete node with its children
   * @param  {Object}   req  [description]
   * @param  {Object}   res  [description]
   * @param  {Function} next [description]
   * @return {Promise}       [description]
   */
  public async delete(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
    const doc = await MenuItem.findById(req.body.id); // short if check
    if (doc) {
      try {
        if (doc) {
          const root = await doc.root();
          if (root) {
            await doc.removeNode(req.body.all);
            return res.json(await root.nestedArray());
          } else {
            return next(boom.badRequest('Document has no root parent'));
          }
        }
      } catch (err) {
        throw boom.badRequest(err);
      }
      return res.json(await MenuItem.nestedArray());
    } else {
      return next(boom.badRequest('No document for delete'));
    }
  }
 
  public async move(req: Request, res: Response, next: NextFunction) {
    //console.log(req.body);
    const dropPosition = req.body.dropPosition;
    try {

      const dropNode: NodeDocument | null = await MenuItem.findById(req.body.dropKey);
      const dragNode: NodeDocument | null = await MenuItem.findById(req.body.dragKey);

      switch (dropPosition) {
        case 0:
          if (dragNode && dropNode) {
            await dragNode.appendTo(dropNode);
          }
          break;
        case -1:
          if (dragNode && dropNode) {
            await dragNode.insertBefore(dropNode);
          }
          break;
        case 1:
          if (dragNode && dropNode) {
            await dragNode.insertAfter(dropNode);
          }
          break;
      }

      if (dragNode) {
        //console.log('dragNode', dragNode);
        const root = await dragNode.root();
        //console.log('root 2', root);
        const nestedArray = await root.nestedArray();
        return res.json(nestedArray);
      }
    } catch (err) {
      console.log(err);
      return next(boom.badRequest(err));
    }

    return next(boom.badRequest('No drag node in database'));

  }

}

export default new MenuItemController();
