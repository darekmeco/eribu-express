import { model, SchemaDefinition } from 'mongoose';
import { NodeSchema } from '@eribu/nestedset-mongoose';
import { HasImages } from '../../media/mongoose/HasImages';
import { NodeDocument, NodeModel } from '@eribu/nestedset-mongoose';
import { Slugify } from '@eribu/mongoose';

const attributes: SchemaDefinition = {
  name: {
    required: [true, 'Wymagane'],
    type: String,
  },
  slug: {
    slug: 'name',
    type: String,
    unique: true,
  },
};

const schema = new NodeSchema(attributes, {
  timestamps: true,
  treeAttribute: 'tree',
});

schema.plugin(HasImages);
schema.plugin(Slugify);
export const MenuItem: NodeModel = model<NodeDocument, NodeModel>('MenuItem', schema);
