const secure = false;
export default [
  { uri: '/menu/index', as: 'menus.menu.index', controller: 'MenuController', action: 'index', options: { secure }, methods: ['get'] },
  { uri: '/menu/store', as: 'menus.menu.store', controller: 'MenuController', action: 'store', options: { secure }, methods: ['post'] },
  { uri: '/menu/edit', as: 'menus.menu.edit', controller: 'MenuController', action: 'edit', options: { secure }, methods: ['get'] },
  { uri: '/item/append', as: 'menus.menu.append', controller: 'MenuItemController', action: 'append', options: { secure }, methods: ['post'] },
  { uri: '/item/move', as: 'menus.menu.move', controller: 'MenuItemController', action: 'move', options: { secure }, methods: ['post'] },
  { uri: '/item/delete', as: 'menus.menu.delete', controller: 'MenuItemController', action: 'delete', options: { secure }, methods: ['post'] },
];
