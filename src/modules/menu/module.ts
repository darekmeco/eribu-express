import { EribuConfig } from '@eribu/modules';
export const Config: EribuConfig = {
  name: 'Menu',
  uri: '/menus',
  active: true,
  version: '0.1',
};
