import CategoryController from '../controllers/CategoryController';
import ProductController from '../controllers/ProductController';
import { EribuRouter } from '@eribu/router';

const productController = new ProductController();
const categoryController = new CategoryController();

const eribuRouter = new EribuRouter();
eribuRouter.get('/info', {controller: productController, method: 'info'});
eribuRouter.get('/products/all', {controller: productController, method: 'all'}, {secure: true});
eribuRouter.get('/products/index', {controller: productController, method: 'index'}, {secure: true});
eribuRouter.post('/products/store', {controller: productController, method: 'store'});
eribuRouter.get('/products/edit', {controller: productController, method: 'edit'});
eribuRouter.post('/products/update', {controller: productController, method: 'update'});
eribuRouter.delete('/products/delete', {controller: productController, method: 'delete'});

eribuRouter.get('/categories/init', {controller: categoryController, method: 'init'});
eribuRouter.get('/categories/tree', {controller: categoryController, method: 'tree'});
eribuRouter.get('/categories/edit', {controller: categoryController, method: 'edit'});
eribuRouter.post('/categories/update', {controller: categoryController, method: 'update'});
eribuRouter.delete('/categories/delete', {controller: categoryController, method: 'delete'});
eribuRouter.post('/categories/move', {controller: categoryController, method: 'move'});
eribuRouter.post('/categories/append', {controller: categoryController, method: 'append'});
eribuRouter.get('/secured', {controller: productController, method: 'secured'}, {secure: true});
export default eribuRouter.getRouter();
