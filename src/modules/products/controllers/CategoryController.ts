import boom from '@hapi/boom';
import { NextFunction, Request, Response } from 'express';
import _get from 'lodash/get';
import _keyBy from 'lodash/keyBy';
import { Category } from '../models/category';
import { NodeDocument } from '@eribu/nestedset-mongoose';

class CategoryController {
  /**
   * Delete node with its children
   * @param  {Object}   req  [description]
   * @param  {Object}   res  [description]
   * @param  {Function} next [description]
   * @return {Promise}       [description]
   */
  public async delete(req: Request, res: Response, next: NextFunction): Promise<Response> {
    const model = await Category.findById(req.body.id); // short if check
    try {
      if (model) {
        await model.removeNode(req.body.all);
      }
    } catch (err) {
      throw boom.badRequest(err);
    }
    return res.json(await Category.nestedArray());
  }
  /**
   * Create node form form and appends to parent
   * @param  {Object}   req  [description]
   * @param  {Object}   res  [description]
   * @param  {Function} next [description]
   * @return      [description]
   */
  public async append(req: Request, res: Response, next: NextFunction): Promise<Response> {

    try {
      const parent: NodeDocument | null = await Category.findById(req.body.parent);
      const child: NodeDocument = new Category(req.body.form); // write validator

      child.validate((error: any) => { // check why not works
        // console.log('append error 2:', error);
      });
      console.log(await Category.hasRoots());
      if (parent) {
        await parent.append(child);
      } else if (await Category.hasRoots() === false) {
        await child.makeRoot();
        return res.json(child);
      } else {

        // emit error ? next(boom.badRequest('Can\'t make roots'));
      }
    } catch (err) {
      next(boom.badRequest(err));
    }

    return res.json({status: 'error'});
  }

  public async edit(req: Request, res: Response) {
    const model = await Category.findById(req.query.id);
    return res.json({
      status: 'success',
      model,
    });
  }

  /**
   * Update product document
   * @param  req [description]
   * @param  res [description]
   * @return     [description]
   */
  public async update(req: Request, res: Response, next: NextFunction) {

    const model: NodeDocument | null = await Category.findById(req.body._id);

    if (model) {
      model.validate(async (error: any) => {
        console.log('error', error);
        if (error == null) {
          console.log('update model', model);
          console.log('up m req.body', req.body);
          try{
            await model.update({
              $set: req.body,
            });
          } catch(err){
            return next(boom.badRequest(err));
          }
        }
        return res.json({
          model,
          status: 'success',
          test: 1,
          error,
          params: req.params,
          body: req.body,
          query: req.query,
        });
      });
    }
  }

  public async move(req: Request, res: Response, next: NextFunction) {
    console.log(req.body);
    const dropPosition = req.body.dropPosition;
    try {

      const dropNode: NodeDocument | null = await Category.findById(req.body.dropKey);
      const dragNode: NodeDocument | null = await Category.findById(req.body.dragKey);

      switch (dropPosition) {
        case 0:
          if (dragNode && dropNode) {
            await dragNode.appendTo(dropNode);
          }
          break;
        case -1:
          if (dragNode && dropNode) {
            await dragNode.insertBefore(dropNode);
          }
          break;
        case 1:
          if (dragNode && dropNode) {
            await dragNode.insertAfter(dropNode);
          }
          break;
      }

    } catch (err) {
      console.log(err);
      return next(boom.badRequest(err));
    }
    return res.json(await Category.nestedArray());
  }

  public async init(req: Request, res: Response, next: NextFunction) {

    console.log('hasRoots: ', await Category.hasRoots());

    let root = null;

    if (await Category.hasRoots() === true) {
      root = await Category.root();
    } else {
      root = new Category({
        name: 'root'
      });
      const err = await root.makeRoot();
      if (_get(err, 'isBoom', false)) {
        return next(err);
      }
    }

    //
    console.log('maked root', root);
    //
    const child1 = new Category({
      name: 'child1',
    });
    await root.append(child1);
    const child2 = new Category({
      name: 'child2',
    });
    await root.append(child2);
    const child3 = new Category({
      name: 'child3',
    });

    await child1.append(child3);
    const child4 = new Category({
      name: 'child4',
    });
    await child3.prepend(child4);
    res.json(req.query);
  }
  public async tree(req: Request, res: Response, next: NextFunction) {

    const category = await Category.find();
    console.log(category, 'CATEGORY');

    return res.json(await Category.nestedArrayAll());
  }
}

export default CategoryController;
