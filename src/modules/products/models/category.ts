import { model } from 'mongoose';
import { NodeSchema } from '@eribu/nestedset-mongoose';
import { HasImages } from '../../media/mongoose/HasImages';
import { NodeModel, NodeDocument } from '@eribu/nestedset-mongoose';
const schema = new NodeSchema({
  name: {
    required: [true, 'Wymagane'],
    type: String,
  },
  slug: {
    slug: 'name',
    type: String,
    unique: true,
  },
}, {
    timestamps: true,
  });
schema.plugin(HasImages);
// schema.plugin(slugify);
export const Category: NodeModel = model<NodeDocument, NodeModel>('ProductCategory', schema);
