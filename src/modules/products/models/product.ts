import mongoose, { Model, model, Document } from 'mongoose';
import validator from 'validator';
import slugify from 'slugify-mongoose';
import { HasImages } from '../../media/mongoose/HasImages';
import { HasStatus } from '@eribu/mongoose';

const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: [true, 'Wymagane'],
  },
  slug: {
    type: String,
    slug: 'name',
    unique: true,
  },
  description: {
    type: String,
    required: [true, 'Wymagane'],
  },
  price: {
    type: Number,
    required: [true, 'Wymagane'],
  },
  categories: {
    type: Array,
  },
}, {
    timestamps: true,
  });

schema.plugin(HasImages);
schema.plugin(HasStatus);
export const Product: Model<Document> = model<Document>('ProductProduct', schema);
