import { EribuConfig } from '@eribu/modules';
export const Config: EribuConfig = {
  name: 'Products',
  uri: '/product',
  active: true,
  version: '0.1',
};
