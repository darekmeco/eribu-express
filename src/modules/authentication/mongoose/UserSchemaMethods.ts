import crypto from 'crypto';
import {Crypto} from '@eribu/helpers';
import jwt from 'jsonwebtoken';
import { UserAuthDocument } from '../interfaces/UserAuthDocument';
import { PasswordData } from '@eribu/helpers';


export class UserSchemaMethods {
  private _id: string;
  private salt: string;
  private hash: string;
  private email: string;
  /**
   * [Hash User password]
   * @param  password [description]
   * @return          [description]
   */
  private async setPassword(this: UserAuthDocument): Promise<void> {
    global.logger.info(`pre save user ${this}`);
    const hash: PasswordData = await Crypto.saltHashPassword(this.password); // TODO: check if this not ovewrite on update
    this.password = hash.passwordHash;
    this.salt = hash.salt;
  }
  /**
   * Validate User password
   * @param  password [description]
   * @return          [description]
   */
  private async validatePassword(this: UserAuthDocument, password: string): Promise<boolean> {
    global.logger.info(`Comparing passwords:
      ${password}:${this.password},
      ${await Crypto.validatePassword(password, this.salt, this.password)}`);
    return await Crypto.validatePassword(password, this.salt, this.password);
  }

  /**
   * [generateJWT description]
   * @return [description]
   */
  private generateJWT(): string {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);
    return jwt.sign({
      email: this.email,
      id: this._id,
      exp: expirationDate.getTime() / 1000,
    }, 'secret');
  }
  /**
   * [toAuthJSON description]
   * @return [description]
   */
  private toAuthJSON(): object {
    return {
      _id: this._id,
      email: this.email,
      token: this.generateJWT(),
    };
  }
}
