import { Schema, SchemaDefinition, SchemaOptions, HookNextFunction, Document } from 'mongoose';
import { MongoError } from 'mongodb';
import { UserSchemaMethods } from './UserSchemaMethods';
import { UserAuthDocument } from '../interfaces/UserAuthDocument';

export class UserSchema extends Schema {
  constructor(definition?: SchemaDefinition, options?: SchemaOptions) {
    super(definition, options);
    this.loadClass(UserSchemaMethods);
    this.addSchemaKeys();
    this.hooks();
  }

  private addSchemaKeys() {
    this.add({
      password: {
        type: String,
        required: [true, 'Required'],
      },
      salt: {
        type: String,
      },
    });
  }

  private async preSave(this: UserAuthDocument, next: HookNextFunction) {
    await this.setPassword();
    next();
  }
  private async preRemove(this: Document) {
    //
  }

  private async postUpdate(this: Document) {
    //
  }
  private async postSave(this: Document, error: MongoError, doc: Document, next: HookNextFunction) {
    //
  }
  private async postRemove(this: Document, error: MongoError, doc: Document, next: HookNextFunction) {
    //
  }

  private hooks() {
    this.pre('save', this.preSave);
    this.pre('remove', this.preRemove);
    this.post('update', this.postUpdate);
    this.post('save', this.postSave);
    this.post('remove', this.postRemove);
  }
}
