import express, { Request, Response, NextFunction } from 'express';
import { Document } from 'mongoose';
import passport from 'passport';
import boom from '@hapi/boom';
import jwt from 'jsonwebtoken';
import { User } from '../../users/models/user';

class AuthController {
  public async register(req: express.Request, res: Response, next: NextFunction) {
    const resp = new Promise((resolve, reject) => {
      passport.authenticate('register', { session: false }, (err, user, info) => {
        console.log('err:', err, user, info);
        if (err == null) {
          return res.json({
            status: 'success',
            message: 'Successfully registered!',
            params: req.params,
            body: req.body,
            query: req.query,
          });
        } else {
          console.log(err, 'errorregister');
          throw next(boom.badRequest(err));
        }

      })(req, res);
    });
    console.log(resp);
  }

  public async login(req: express.Request, res: Response, next: NextFunction) {
    global.logger.info('Start login');
    await passport.authenticate('login', async (err, user, info: { message: string }) => {
      try {
        console.log('user:', user);
        // if (err) {
        //   throw boom.badRequest(err);
        // }
        if (!user) {
          return next(boom.notFound(info.message));
        }
        req.login(user, { session: false }, async (error) => {
          if (error) {
            console.log(error);
            throw boom.badRequest(error);
          }
          // We don't want to store the sensitive information such as the
          // user password in the token so we pick only the id
          const body = { _id: user._id };
          // Sign the JWT token and populate the payload with the user id
          const token = jwt.sign(body, 'top_secret');
          // Send back the token to the user
          return res.json({ token });
        });
      } catch (error) {
        global.logger.info('Login catched error');
        console.log(error); // delete me
        throw boom.badRequest(error);
      }
    })(req, res);

  }
  /**
   * Ensure we dont ovewrite an document id
   */
  //   Reflect.deleteProperty(req.body, '_id');
  //   const model = new User(req.body);
  //   model.validate(async (error: any) => {
  //     console.log('error', error);
  //     if (error == null) {
  //       const savedModel: Document = await model.save();
  //       return res.json({
  //         body: req.body,
  //         model: savedModel,
  //         params: req.params,
  //         query: req.query,
  //         status: 'success',
  //       });
  //     }
  //     return res.json({
  //       body: req.body,
  //       error,
  //       params: req.params,
  //       query: req.query,
  //       status: 'error',
  //     });
  //   });
  // }

}

export default AuthController;
