import { Document } from 'mongoose';

export interface UserAuthDocument extends Document {
  // [key: string]: any;
  password: string;
  salt: string;
  setPassword(): void;
  validatePassword(password: string): boolean;
}
