import passport from 'passport';
import passportLocal from 'passport-local';
import passportJwt, { JwtFromRequestFunction } from 'passport-jwt';

import { User } from '../../users/models/user';
import { UserAuthDocument } from '../interfaces/UserAuthDocument';

export class AuthProvider {

  private LocalStrategy = passportLocal.Strategy;
  private JWTstrategy = passportJwt.Strategy;
  private ExtractJWT = passportJwt.ExtractJwt;

  constructor() {
    global.logger.info('Registering AuthProvider');
    this.registerStrategies();
  }

  private registerStrategies() {
    /**
     * LocalStrategy for register
     * @param  'signup'                        [description]
     * @param  newLocalStrategy({usernameField [description]
     * @return                                 [description]
     */
    passport.use('register', new this.LocalStrategy({
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    }, async (req, _email, _password, done) => {
      try {
        console.log('signup', req.body);
        // Save the information provided by the user to the the database
        const user = await User.create(req.body);
        // Send the user information to the next middleware
        return done(null, user);
      } catch (error) {
        done(error);
      }
    }));

    /**
     * LocalStrategy for login
     * @param  'login'                         [description]
     * @param  newLocalStrategy({usernameField [description]
     * @return                                 [description]
     */
    passport.use('login', new this.LocalStrategy({
      usernameField: 'email',
      passwordField: 'password',
    }, async (email, password, done) => {
      try {
        global.logger.info(`Checking credentials: ${email}:${password}`);
        // Find the user associated with the email provided by the user
        const user: UserAuthDocument | null = await User.findOne({ email });
        if (!user) {
          // If the user isn't found in the database, return a message
          return done(null, false, { message: 'User Document not found' });
        }
        // Validate password and make sure it matches with the corresponding hash stored in the database
        // If the passwords match, it returns a value of true.
        const validate: boolean = await user.validatePassword(password);
        if (!validate) {
          return done(null, false, { message: 'Wrong Password' });
        }
        // Send the user information to the next middleware
        return done(null, user, { message: 'Logged in Successfully' });
      } catch (error) {
        return done(error);
      }
    }));

    passport.use(new this.JWTstrategy({
      // secret we used to sign our JWT
      secretOrKey: 'top_secret',
      jwtFromRequest: this.getJWT(),
    }, async (token, done) => {
      try {
        // Pass the user details to the next middleware
        console.log(token);
        global.logger.info(`got token: ${token}`);
        return done(null, token.user);
      } catch (error) {
        done(error);
      }
    }));

  }

  private getJWT(): JwtFromRequestFunction {
    return this.ExtractJWT.fromAuthHeaderAsBearerToken();
  }

}
