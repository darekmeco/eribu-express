import AuthController from '../controllers/AuthController';
import { EribuRouter } from '@eribu/router';
const authController = new AuthController();
const eribuRouter = new EribuRouter();
eribuRouter.post('/register', { controller: authController, method: 'register' });
eribuRouter.post('/login', { controller: authController, method: 'login' });

export default eribuRouter.getRouter();
