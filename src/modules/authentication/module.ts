import { EribuConfig } from '@eribu/modules';
export const Config: EribuConfig = {
  name: 'Authentification',
  uri: '/auth',
  active: true,
  version: '0.1',
};
