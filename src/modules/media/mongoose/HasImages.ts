import { Schema } from "mongoose";
import File from "../../media/models/file";
export function HasImages(schema: Schema) {
  schema.add({
    singleFiles: [File.schema],
    multipleFiles: [{
      kind: String,
      files: [File.schema],
    }],
  });
}
