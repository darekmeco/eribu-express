import { EribuConfig } from '@eribu/modules';
export const Config: EribuConfig = {
  name: 'Media',
  active: false,
  version: '0.2',
  uri: '/files',
};
