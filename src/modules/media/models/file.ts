import mongoose from 'mongoose';
import validator from 'validator';

const Schema = mongoose.Schema;

const schema = new Schema({
  kind: {
    required: false,
    type: String,
  },
  filename: {
    required: [true, 'Wymagane'],
    type: String,
  },
  path: {
    required: [true, 'Wymagane'],
    type: String,
  },
});

export default mongoose.model('File', schema);
