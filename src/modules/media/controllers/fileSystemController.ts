import { FileSystem } from '@eribu/filesystem';
import fs from 'fs';
import path from 'path';
import { File } from '@eribu/filesystem';
import { Request, Response } from 'express';
import _chunk from 'lodash/chunk';
import _size from 'lodash/size';
import _get from 'lodash/get';

class FileSystemController {

  constructor() {
    console.log(File);
  }
  /**
   * Get all fs tree
   * @param  req [description]
   * @param  res [description]
   * @return     [description]
   */
  public all(req: Request, res: Response): any {
    const currentDir: string = './';
    res.json({
      files: this.read(currentDir),
    });
  }
  /**
   * Reads directory structure as array.
   * @param  req [Request]
   * @param  res [Response]
   * @return     [json]
   */
  public index(req: Request, res: Response) {
    return res.json(FileSystem.readFoldersTree());
  }

  public files(req: Request, res: Response) {

    console.log('files req.body: ', req.body);
    /**
     * Current page number from filesystem
     */
    const page: number = req.body.current && req.body.current > 0 ? req.body.current - 1 : 1;

    if (typeof (req.body.current) === 'number') {
      const allFiles = FileSystem.readFilesWithPath(req.body.path);
      const total = _size(allFiles);
      const perPage = typeof (req.body.perPage) === 'number' ? req.body.perPage : 10;

      const filesPage: any[] = _get(
        _chunk(
          FileSystem.readFilesWithPath(req.body.path), perPage,
        ), page, []);

      return res.json({
        files: filesPage,
        total,
      });
    }
    return res.sendStatus(404);
  }

  public delete(req: Request, res: Response) {
    const filename = req.body.filename;
    const searchpath = req.body.searchpath;
    FileSystem.deleteFileWithPath(searchpath, filename);
    res.json({ status: 'success' });
  }
  public store(req: Request, res: Response) {
    res.json({});
  }

  /**
   * Reads dir and makes recursive array structure
   * https://gitlab.com/darekmeco/eribu-express/snippets/1813383
   * @param  dir durectory path
   * @return array
   */
  private read(dir: string) {
    const dirContent: string[] = fs.readdirSync(dir);
    const data = dirContent.reduce((files: string[], file: string): string[] => {
      const dirent = fs.statSync(path.join(dir, file));
      if (dirent.isDirectory()) {
        console.log(path.join(dir, file));
        return files.concat(this.read(path.join(dir, file)));
      } else {
        return files.concat(path.join(dir, file));
      }
    }, []);
    return data;
  }
}

export default FileSystemController;
