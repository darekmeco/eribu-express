import { Response, Request } from 'express';
import { UploadedFile } from 'express-fileupload';
import boom from '@hapi/boom';
class FileController {
  async index(req: Request, res: Response) {
    const files = {
      a: 1
    };
    res.json(files);
  }
  async store(req: Request, res: Response) {
    res.json({});
  }
  upload(req: Request, res: Response) {
    if (req.files) {
      // const file: UploadedFile | UploadedFile[] = req.files.file;

      const files: UploadedFile[] = Array.isArray(req.files.file) ? req.files.file : [req.files.file];

      const path = req.body.path;

      for (let file of files) {

        console.log('file!!:', file);
        const dir = global.assetsDir + '/' + path + '/' + file.name;
        console.log(dir);
        console.log(req.files);
        file.mv(dir, (err: any) => {
          if (err) {
            throw boom.badRequest(err);
          }
        });
      }

      return res.json({status: 'success'});

    } //Todo: make answer with error
  }
}

export default FileController;
