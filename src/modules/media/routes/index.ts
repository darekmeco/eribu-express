import express from 'express';
import FileController from '../controllers/fileController';
import FileSystemController from '../controllers/fileSystemController';
const router = express.Router();

const fileController = new FileController();
const fileSystemController = new FileSystemController();

router.get('/', fileController.index);
router.post('/store', fileController.store);

router.post('/upload', fileController.upload);
router.put('/upload', fileController.upload);

router.get('/filesystem/index', fileSystemController.index);
router.post('/filesystem/files', fileSystemController.files);
router.delete('/filesystem/files', fileSystemController.delete);

export default router;
