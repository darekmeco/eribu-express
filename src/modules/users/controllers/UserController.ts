import express, { Request, Response, NextFunction } from 'express';
import { User } from '../models/user';
import { Document } from 'mongoose';
import { UserAuthDocument } from '../../authentication/interfaces/UserAuthDocument';
import _chunk from 'lodash/chunk';
import { badRequest } from '@hapi/boom';
import _set from 'lodash/set';

class UserController {

  public async index(req: Request, res: Response) {
    console.log(req.query, 'req-query');
    let sorter = {};
    _set(sorter, req.query.field ? req.query.field : 'createdAt', req.query.orderBy == 'ascend' ? 1 : -1);
    const models = await User.find({
      $or: [
        {
          firstname: {
            $regex: req.query.searchInput,
            $options: 'ig',
          },
        },
        {
          lastname: {
            $regex: req.query.searchInput,
            $options: 'ig',
          },
        },
        {
          email: {
            $regex: req.query.searchInput,
            $options: 'ig',
          },
        }
      ]
    }).sort(sorter);
    const modelsForPage = _chunk(models, req.query.pageSize)[req.query.current - 1];
    res.json({
      results: modelsForPage,
      pagination: {
        total: models.length,
        page: req.query.current
      }
    });
  }

  public async store(req: express.Request, res: Response, next: NextFunction) {
    /**
     * Ensure we dont ovewrite an document id
     */
    Reflect.deleteProperty(req.body, '_id');
    const model = new User(req.body);

    model.validate(async (error: any) => {
      console.log('error', error);
      if (error == null) {
        try {
          const savedModel: Document = await model.save();
          return res.json({
            body: req.body,
            model: savedModel,
            params: req.params,
            query: req.query,
            status: 'success',
          });
        } catch (err) {
          next(badRequest(err));
        }

      }
      return res.json({
        body: req.body,
        error,
        params: req.params,
        query: req.query,
        status: 'error',
      });
    });
  }
  public async edit(req: express.Request, res: express.Response) {
    const model = await User.findById(req.query.id);
    return res.json({
      status: 'success',
      model,
    });
  }
  /**
   * Update role document
   * @param  req [description]
   * @param  res [description]
   * @return     [description]
   */
  public async update(req: express.Request, res: Response, next: NextFunction) {
    const model: Document | null = await User.findById(req.body._id);
    if (model) {
      model.validate(async (error: any) => {
        if (error == null) {
          try {
            await model.update({
              $set: req.body,
            });
          } catch (err) {
            next(badRequest(err));
          }
        }
        return res.json({
          model,
          status: 'success',
          test: 1,
          error,
          params: req.params,
          body: req.body,
          query: req.query,
        });
      });
    }
  }
  public async delete(req: express.Request, res: Response) {
    const model: Document | null = await User.findById(req.query.id);
    if (model) {
      model.remove();
      return res.json({
        status: 'success',
        model,
      });
    }
  }
  public async changePassword(req: express.Request, res: Response) {
    const model: UserAuthDocument | null = await User.findById(req.body._id);
    if (model) {
      if (await model.validatePassword(req.body.current_password)) {
        console.log(req.body, 'change password model');
        model.password = req.body.password;
        await model.save();

        return res.json({
          errors: false,
          message: 'The password has been successfully changed',
          model,
          body: req.body,
        });

      } else {
        return res.json({
          errors: true,
          message: 'Invalid password',
        });
      }
    }
  }

  public async init(req: express.Request, res: Response, next: NextFunction) {

    const model = new User({email: 'admin@admin.com', password: "admin"});

    model.validate(async (error: any) => {
      console.log('error', error);
      if (error == null) {
        try {
          const savedModel: Document = await model.save();
          return res.json({
            body: req.body,
            model: savedModel,
            params: req.params,
            query: req.query,
            status: 'success',
          });
        } catch (err) {
          next(badRequest(err));
        }

      }
      return res.json({
        body: req.body,
        error,
        params: req.params,
        query: req.query,
        status: 'error',
      });
    });
  }

}

export default UserController;
