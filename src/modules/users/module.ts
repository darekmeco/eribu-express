import { EribuConfig } from '@eribu/modules';
export const Config: EribuConfig = {
  name: 'Users',
  uri: '/user',
  active: true,
  version: '0.1',
};
