import UserController from '../controllers/UserController';
import RoleController from '../controllers/RoleController';
import { EribuRouter } from '@eribu/router';
const userController = new UserController();
const roleController = new RoleController();

const eribuRouter = new EribuRouter();
// user
eribuRouter.get('/users/init', {controller: userController, method: 'init'});
eribuRouter.get('/users/index', {controller: userController, method: 'index'});
eribuRouter.post('/users/store', {controller: userController, method: 'store'});
eribuRouter.get('/users/edit', {controller: userController, method: 'edit'});
eribuRouter.post('/users/update', {controller: userController, method: 'update'});
eribuRouter.delete('/users/delete', {controller: userController, method: 'delete'});
eribuRouter.post('/users/change-password', {controller: userController, method: 'changePassword'});

// role
eribuRouter.get('/roles/all', {controller: roleController, method: 'all'});
eribuRouter.get('/roles/index', {controller: roleController, method: 'index'});
eribuRouter.post('/roles/store', {controller: roleController, method: 'store'});
eribuRouter.get('/roles/edit', {controller: roleController, method: 'edit'});
eribuRouter.post('/roles/update', {controller: roleController, method: 'update'});
eribuRouter.delete('/roles/delete', {controller: roleController, method: 'delete'});

export default eribuRouter.getRouter();
