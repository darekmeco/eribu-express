import mongoose, { Model, model, Document } from 'mongoose';
import validator from 'validator';

const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: [true, 'Wymagane'],
  },
  slug: {
    type: String,
    required: [true, 'Wymagane'],
    unique: true,
  },
}, {
    timestamps: true,
  });
export const Role: Model<Document> = model<Document>('Role', schema);
