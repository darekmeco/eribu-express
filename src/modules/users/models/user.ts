import mongoose, { Model, model } from 'mongoose';
import { UserSchema } from '../../authentication/mongoose/UserSchema';
import { Role } from './role';
import { HasImages } from '../../media/mongoose/HasImages';
import { UserAuthDocument } from '../../authentication/interfaces/UserAuthDocument';

const schema = new UserSchema({

  firstname: {
    type: String,
  },
  lastname: {
    type: String,
  },
  email: {
    type: String,
    required: [true, 'Wymagane'],
    unique: true,
  },
  description: {
    type: String,
  },
  role: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Role',
  },
}, {
    timestamps: true,
  });

schema.plugin(HasImages);
export const User: Model<UserAuthDocument> = model<UserAuthDocument>('User', schema);
