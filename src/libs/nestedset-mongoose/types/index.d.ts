import { SchemaOptions, Document, Model, Schema } from 'mongoose';
import { NodeSchema } from '../classes/NodeSchema';

export interface NodeSchemaAttributes {
  depth: string;
  lft: string;
  rgt: string;
}
export interface NodeSchemaActions {
  append: string;
  appendTo: string;
  prepend: string;
  prependTo: string;
  insertBefore: string;
  insertAfter: string;
  remove: string;
  removeAll: string;
  makeRoot: string;
}
export interface NodeSchemaOptions extends SchemaOptions {
  treeAttribute?: string;
}
export interface NodeDocument extends Document {
  [key: string]: any;
  children: NodeDocument[];
  schema: NodeSchema;
  nodeModel: NodeDocument | null;
  lft: number;
  rgt: number;
  depth: number;
  action: string | null;
  constructor: NodeModel;
  creating: boolean;
  removeNode(node: NodeDocument): any;
  moveNode(node: NodeDocument, depth: number): any;
  prepend(node: NodeDocument): any;
  append(node: NodeDocument): any;
  beforeInsertNode(value: number, depth: number): any;
  makeRoot(): any;
  saveRoot(): any;
  appendTo(node: NodeDocument): any;
  isLeaf(): boolean;
}
export interface NodeObject {
  [key: string]: any;
  title: string;
  name: string;
  children: NodeObject[];
  key: string;
  value: string;
  state: object;
  lft: number;
  rgt: number;
  depth: number;
}
export interface NodeModel extends Model<NodeDocument> {
  getNestedArrayObjectCollection(collection: NodeDocument[]): NodeObject[];
  getNodeObjectCollectionParent(nodeObject: NodeObject, collection: any): NodeObject | undefined;
  nestedArray(): NodeDocument[];
  nestedArrayAll(): NodeDocument[];
  root(): NodeDocument;
  roots(): NodeDocument[];
  hasRoots(): boolean;
  getCollectionParent(tree: NodeObject[]): NodeObject | undefined;
}
