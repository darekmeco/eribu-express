import { Arr } from '@eribu/helpers';
import boom from '@hapi/boom';
import _remove from 'lodash/remove';
import { NodeObject, NodeModel, NodeDocument } from '@eribu/nestedset-mongoose';
import { NestedDocumentQueries } from './NestedDocumentQueries';
import { MenuItem } from './../../../modules/menu/models/MenuItem';
import { HookNextFunction } from 'mongoose';

export class DocumentMethods {

  /**
   *  Get array of root Node Documents
   * @param  this [description]
   * @return {Promise<NodeDocument[]>}
   */
  public static async roots(this: NodeDocument): Promise<NodeDocument[]> {
    const roots: NodeDocument[] = await this.find({
      [this.schema.attributes.lft]: 1,
    }); // check order on yii nested?
    return roots;
  }

  /**
   * Checks if roots exists
   * @return {Promise<boolean>}
   */
  public static async hasRoots(this: NodeDocument): Promise<boolean> {
    return Arr.notEmpty(await this.roots());
  }

  /**
   * Generate neste array of tree structure
   * @return {[]} [description]
   */
  public static async nestedArrayAll(this: NodeModel): Promise<NodeObject[]> {

    const docs = await this.find().sort({
      lft: 'asc',
    });

    const tree: NodeObject[] = [];

    docs.forEach((doc: NodeDocument) => {
      const node: NodeObject = doc.toObject();
      node.title = `${node.name}`; // ${node.name} (${model.isLeaf()}) (lft: ${node.lft} rgt: ${node.rgt}) (${node._id});
      node.children = [];
      node.key = node._id;
      node.value = node._id;
      node.state = {
        expanded: true,
      };
      tree.push(node);
    });

    const childKeys: number[] = [];

    tree.forEach((node: NodeObject) => {
      const parent: NodeObject | undefined = this.getNodeObjectCollectionParent(node, tree);
      if (parent) {
        parent.children.push(node);
        childKeys.push(node._id);
      }
    });

    _remove(tree, (o: NodeObject) => {
      //console.log('o.id', childKeys.includes(o._id));
      return childKeys.includes(o._id);
    });

    return tree;
  }
  public isRoot(this: NodeDocument): boolean {
    return this[this.schema.attributes.lft] === 1;
  }
  /**
   * Get one root Node Document
   * @return {Promise<NodeDocument>} [description]
   */
  public async root(this: NodeDocument): Promise<NodeDocument | null> {
    const condition: any = {
      [this.schema.attributes.lft]: 1,
    };

    //console.log(condition);
    if (this.schema.treeAttribute) {
      condition[this.schema.treeAttribute] = this[this.schema.treeAttribute];
    }
    const root = await this.constructor.findOne(condition);
    //console.log('root 1', root);
    return root;
  }
  /**
   * Get parents collection
   * @param  this  NodeDocument
   * @param  depth number
   * @return  {NodeDocument[]}
   */
  public async parents(this: NodeDocument, depth: number | null = null): Promise<NodeDocument[]> {
    const condition = NestedDocumentQueries.parentsQuery(this, depth);
    const parents: NodeDocument[] = await this.constructor.find(condition).sort({ lft: 'asc' }); // check order on yii nested?
    return parents;
  }
  /**
   * Get Parent of node
   * @param  this [description]
   * @return      [description]
   */
  public async parent(this: NodeDocument): Promise<NodeDocument | null> {
    const condition = NestedDocumentQueries.parentsQuery(this);
    const parent: NodeDocument | null = await this.constructor.findOne(condition).sort({ lft: 'asc' });
    return parent;
  }
  /**
   * Get children of current node as array
   * @param  this [description]
   * @return      [description]
   */
  public async getChildren(this: NodeDocument): Promise<NodeDocument[]> {
    const leftValue = this[this.schema.attributes.lft];
    const rightValue = this[this.schema.attributes.rgt];
    //console.log('leftValue', leftValue);
    //console.log('rightValue', rightValue);

    const condition: any = {
      [this.schema.attributes.lft]: {
        $gt: leftValue,
      },
      [this.schema.attributes.rgt]: {
        $lt: rightValue,
      },
    };

    this.applyTreeAttributeCondition(condition);

    const children: NodeDocument[] = await this.constructor.find(condition).sort({
      lft: 'asc',
    }); // check order on yii nested?
    return children;
  }

  public async getChildrenAndSelf(this: NodeDocument): Promise<NodeDocument[]> {
    let children: NodeDocument[] = [];
    children.push(this);
    children = children.concat(await this.getChildren());
    // console.log('children', children);
    return children;
  }
  /**
   * Get Nested Array for current Node Document
   * @param  this [NodeDocument]
   * @return      [NodeObject[]]
   */
  public async nestedArray(this: NodeDocument): Promise<NodeObject[]> {
    // console.log('menu', this);
    let tree: NodeObject[] = [];
    const childrenAndSelf = await this.getChildrenAndSelf();
    tree = this.constructor.getNestedArrayObjectCollection(childrenAndSelf);
    return tree;
  }
  /**
   * Logic for pre save hook
   * @param  this [description]
   * @return {void} [description]
   */
  public async beforeInsertRootNode(this: NodeDocument) {
    // console.log('beforeInsertRootNode');

    this[this.schema.attributes.lft] = 1;
    this[this.schema.attributes.rgt] = 2;
    this[this.schema.attributes.depth] = 0;
    this.creating = true;
  }
  public async beforeInsertNode(this: NodeDocument, value: number, depth: number) {
    if (this.nodeModel) {
      // console.log('beforeInsertNode', value, depth, this.nodeModel[this.schema.attributes.depth]);
      // console.log('this.schema.treeAttribute', this.schema.treeAttribute);
      // TODO: check if appending to parent ? - done before
      this[this.schema.attributes.lft] = value;
      this[this.schema.attributes.rgt] = value + 1;
      this[this.schema.attributes.depth] = this.nodeModel[this.schema.attributes.depth] + depth;

      if (this.schema.treeAttribute) {
        this[this.schema.treeAttribute] = this.nodeModel[this.schema.treeAttribute];
      }
      await this.shiftLeftRightAttribute(value, 2);
    }
  }
  /**
   * Make root from node
   * @param  this [description]
   * @return      [description]
   */
  public async makeRoot(this: NodeDocument) {
    if (Arr.notEmpty(await this.constructor.roots()) && !this.schema.treeAttribute) {
      throw boom.badRequest('Can not create more than one root when "treeAttribute" is false.');
    }
    this.action = this.schema.actions.makeRoot;
    //console.log('tree attribute:', this.schema.treeAttribute);
    //console.log('tree roots:', await this.constructor.roots());
    //console.log('makeRoot:', this);
  }
  /**
   * Save prepared root node
   * @param  this [description]
   * @return      [description]
   */
  public async saveRoot(this: NodeDocument): Promise<NodeDocument> {
    await this.makeRoot();
    return await this.save();
  }
  /**
   * Appends Child node to Current node
   * @param  {Model} node Child Model mongoose
   */
  public async append(this: NodeDocument, node: NodeDocument): Promise<NodeDocument> {
    node.action = node.schema.actions.append;
    node.nodeModel = this;
    return await node.save();
  }
  /**
   * Remove current node from tree
   * @param  this [description]
   * @param  all  [description]
   * @return      [description]
   */
  public async removeNode(this: NodeDocument, all: boolean = false) {
    this.action = all ? this.schema.actions.removeAll : this.schema.actions.remove;
    if (this.action === this.schema.actions.removeAll) {
      const leftValue = this[this.schema.attributes.lft];
      const rightValue = this[this.schema.attributes.rgt];
      const condition = {
        [this.schema.attributes.lft]: {
          $gte: leftValue,
        },
        [this.schema.attributes.rgt]: {
          $lte: rightValue,
        },
      };
      await this.constructor.deleteMany(condition);
    } else {
      await this.remove();
    }
  }
  /**
   * Insert this before node
   * @param  node [description]
   * @return      [description]
   */
  public async insertBefore(this: NodeDocument, node: NodeDocument): Promise<NodeDocument> {
    if (node.isRoot() === true) {
      throw boom.badRequest('Can\'t move root.');
    } else {
      this.action = node.schema.actions.insertBefore;
      this.nodeModel = node;
      return await this.save();
    }
  }
  /**
   * Insert this after node
   * @param  node [description]
   * @return      [description]
   */
  public async insertAfter(this: NodeDocument, node: NodeDocument): Promise<NodeDocument> {
    if (node.isRoot() === true) {
      throw boom.badRequest('Can\'t move root.');
    } else {
      this.action = node.schema.actions.insertAfter;
      this.nodeModel = node;
      return await this.save();
    }
  }
  /**
   * Appends Current node to Parent node
   * @param  {Model} node Parent Model mongoose
   */
  public async appendTo(this: NodeDocument, node: NodeDocument): Promise<void> {
    this.action = node.schema.actions.appendTo;
    this.nodeModel = node;
    await this.save();
  }
  private async moveNode(this: NodeDocument, value: number, depth: number) {

    if (this.nodeModel) {

      const treeAttribute = this.schema.treeAttribute;
      const depthAttribute = this.schema.attributes.depth;
      const leftAttribute = this.schema.attributes.lft;
      const rightAttribute = this.schema.attributes.rgt;

      let {
        [leftAttribute]: leftValue = null,
        [rightAttribute]: rightValue = null,
      } = this;

      const { [depthAttribute]: depthValue = null } = this;

      const newdepth: number = this.nodeModel[depthAttribute] - depthValue + depth;

      if (!treeAttribute || (this[treeAttribute] === this.nodeModel[treeAttribute])) {
        const delta = rightValue - leftValue + 1;
        await this.shiftLeftRightAttribute(value, delta);

        let docthis = await MenuItem.findById(this._id);
        let docnode = await MenuItem.findById(this.nodeModel._id);

        console.log('post shift1 this :', docthis);
        console.log('post shift1 this action :', this.action);
        console.log('post shift1 nodeModel:', docnode);

        if (leftValue >= value) {
          leftValue += delta;
          rightValue += delta;
        }
        const c1 = {
          [leftAttribute]: {
            $gte: leftValue,
          },
          [rightAttribute]: {
            $lte: rightValue,
          },
        };
        this.applyTreeAttributeCondition(c1);
        await this.constructor.updateMany(c1, {
          $inc: {
            [depthAttribute]: newdepth,
          },
        });

        let docthis1 = await MenuItem.findById(this._id);
        let docnode1 = await MenuItem.findById(this.nodeModel._id);

        console.log('post update 1  this :', docthis1);
        console.log('post update 1  this action :', this.action);
        console.log('post update 1  nodeModel:', docnode1);


        for (const attr of [leftAttribute, rightAttribute]) {
          const c2 = {
            [attr]: {
              $gte: leftValue,
            },
            [attr]: {
              $lte: rightValue,
            },
          };

          this.applyTreeAttributeCondition(c2);

          await this.constructor.updateMany(c2, {
            $inc: {
              [attr]: (value - leftValue),
            },
          });

          let docthis2 = await MenuItem.findById(this._id);
          let docnode2 = await MenuItem.findById(this.nodeModel._id);

          console.log('post update 2  this :', docthis2);
          console.log('post update 2  this action :', this.action);
          console.log('post update 2  nodeModel:', docnode2);



        }

        await this.shiftLeftRightAttribute(rightValue + 1, -delta);

        let docthis3 = await MenuItem.findById(this._id);
        let docnode3 = await MenuItem.findById(this.nodeModel._id);

        console.log('post shift2 this :', docthis3);
        console.log('post shift2 this action :', this.action);
        console.log('post shift2 nodeModel:', docnode3);



        //console.log('move node leftValue', leftValue);
      } else {

        const destTreeAttribute = this.nodeModel[treeAttribute];
        for (const attr of [leftAttribute, rightAttribute]) {
          const c1 = {
            [attr]: { $gte: value },
            [treeAttribute]: { $eq: destTreeAttribute },
          };
          await this.constructor.updateMany(c1, {
            $inc: {
              [attr]: (rightValue - leftValue + 1),
            },
          });

          const delta = value - leftValue;

          const c2 = {
            [leftAttribute]: {
              $gte: leftValue,
            },
            [rightAttribute]: {
              $lte: rightValue,
            },
            [treeAttribute]: {
              $lte: rightValue,
            },
          };

          await this.constructor.updateMany(c2, {
            $inc: {
              [leftAttribute]: (rightValue - leftValue + 1),
              [rightAttribute]: (rightValue - leftValue + 1),
              [depthAttribute]: (rightValue - leftValue + 1),
              [treeAttribute]: destTreeAttribute,
            },
          });

          await this.shiftLeftRightAttribute(rightValue + 1, -2);

        }
      }
      // const leftValue = this[this.schema.attributes.lft];
      // $rightValue = $this -> owner -> getAttribute($this -> rightAttribute);
      // $depthValue = $this -> owner -> getAttribute($this -> depthAttribute);
      // $depthAttribute = $db -> quoteColumnName($this -> depthAttribute);
      // $depth = $this -> node -> getAttribute($this -> depthAttribute) - $depthValue + $depth;
    }
  }
  private async shiftLeftRightAttribute(this: NodeDocument, value: number, delta: number) {

    for (const attr of [this.schema.attributes.lft, this.schema.attributes.rgt]) {
      const condition = {
        [attr]: {
          $gte: value,
        },
      };

      const c = this.applyTreeAttributeCondition(condition);

      await this.constructor.updateMany(c, {
        $inc: {
          [attr]: delta,
        },
      });
    }
  }

  private applyTreeAttributeCondition(this: NodeDocument, condition: any): object {
    if (this.schema.treeAttribute && this[this.schema.treeAttribute]) {
      condition[this.schema.treeAttribute] = { $eq: this[this.schema.treeAttribute] };
    }
    return condition;
  }
  /**
   * Before update logic
   * @param  this [description]
   * @param  next [description]
   * @param  err  [description]
   * @return      [description]
   */
  private async beforeUpdate(this: NodeDocument, next: HookNextFunction, err: any) {
    console.log('before update this :', this);
    console.log('before update this action :', this.action);
    console.log('before update nodeModel:', this.nodeModel);

    if (this.nodeModel) {
      switch (this.action) {
        case this.schema.actions.appendTo:
          if (this.nodeModel.isNew) {
            throw boom.badRequest('Can not move a node when the target node is new record.');
          }
          if (this._id.equals(this.nodeModel._id)) {
            throw boom.badRequest('Can not move a node when the target node is same.');
          }
          const parent = await this.parent();
          if (parent && this._id.equals(parent._id)) {
            throw boom.badRequest('Can not move a node when the target node is child..');
          }
          break;
      }
    }
  }
  private async beforeCreate(this: NodeDocument, next: HookNextFunction, err: any) {
    console.log('before create this :', this);
    console.log('before create this action :', this.action);
    console.log('before create nodeModel:', this.nodeModel);

    if (this.nodeModel) {
      switch (this.action) {
        case this.schema.actions.append:
          await this.beforeInsertNode(this.nodeModel.rgt, 1);
          break;
        case this.schema.actions.appendTo:
          await this.beforeInsertNode(this.nodeModel.rgt, 1);
          break;
        case this.schema.actions.prepend:
          await this.beforeInsertNode(this.nodeModel.lft + 1, 1);
          break;
        case this.schema.actions.insertBefore:
          // console.log('attrs:', this.attributes);
          await this.beforeInsertNode(this.nodeModel[this.schema.attributes.lft], 0);
          break;
        case this.schema.actions.insertAfter:
          // console.log('attrs:', this.attributes);
          await this.beforeInsertNode(this.nodeModel[this.schema.attributes.rgt] + 1, 0);
          break;
      }
    } else {
      switch (this.action) {
        case this.schema.actions.makeRoot:
          await this.beforeInsertRootNode();
          // console.log('beforeInsertRootNode next 2', this);
          next();
          break;
      }
    }
  }
  public async afterUpdate(this: NodeDocument, next: HookNextFunction) {

   if (this.nodeModel) {
      switch (this.action) {
        case this.schema.actions.appendTo:
          const rightValue = this.nodeModel[this.schema.attributes.rgt];
          await this.moveNode(rightValue, 1);

          let docthis1 = await MenuItem.findById(this._id);
          let docnode1 = await MenuItem.findById(this.nodeModel._id);

          console.log('after update this :', docthis1);
          console.log('after update this action :', this.action);
          console.log('after update nodeModel:', docnode1);
          break;
      }
   }
  }

}
