import { Schema } from 'mongoose';
import { NodeSchemaAttributes, NodeSchemaActions} from '@eribu/nestedset-mongoose';
export interface NodeSchema extends Schema {
  treeAttribute?: string;
  attributes: NodeSchemaAttributes;
  actions: NodeSchemaActions;
}
