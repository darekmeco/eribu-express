import { NodeDocument } from "@eribu/nestedset-mongoose";
export class NestedDocumentQueries {
  public static parentsQuery(document: NodeDocument, depth: number | null = null) {

    const leftValue = document[document.schema.attributes.lft];
    const rightValue = document[document.schema.attributes.rgt];
    const depthValue = document[document.schema.attributes.depth];

    const condition: any = {
      [document.schema.attributes.lft]: {
        $lt: leftValue,
      },
      [document.schema.attributes.rgt]: {
        $gt: rightValue,
      },
    };

    if (depth !== null) {
      condition[document.schema.attributes.depth] = { $gte: depthValue - depth };
    }
    document.applyTreeAttributeCondition(condition);

    return condition;
  }
}
