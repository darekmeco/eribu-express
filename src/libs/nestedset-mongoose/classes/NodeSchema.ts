import { Schema, SchemaDefinition, HookNextFunction } from 'mongoose';
import { MongoError } from 'mongodb';
import { NodeSchemaAttributes, NodeSchemaOptions, NodeSchemaActions, NodeDocument } from '@eribu/nestedset-mongoose';
import { DocumentMethods } from './DocumentMethods';
import { NodeCollection } from './NodeCollection';
import { HooksMethods } from './HooksMethods';
import { MenuItem } from '../../../modules/menu/models/MenuItem';
import boom from '@hapi/boom';

export class NodeSchema extends Schema {
  /**
   * Attribute name for multi root treesr
   */
  public treeAttribute?: string;
  /**
   * Default DB attributes
   */
  public actions: NodeSchemaActions = Object.freeze({
    append: 'append',
    appendTo: 'append-to',
    prepend: 'prepend',
    prependTo: 'prepend-to',
    insertBefore: 'insert-before',
    insertAfter: 'insert-after',
    remove: 'remove',
    removeAll: 'remove-all',
    makeRoot: 'make-root',
  });
  public attributes: NodeSchemaAttributes = Object.freeze({
    depth: 'depth',
    lft: 'lft',
    rgt: 'rgt',
  });
  constructor(definition?: SchemaDefinition, options?: NodeSchemaOptions) {
    super(definition, options);

    this.add({
      [this.attributes.lft]: {
        min: 1,
        type: Number,
      },
      [this.attributes.rgt]: {
        min: 1,
        type: Number,
      },
      [this.attributes.depth]: {
        min: 0,
        type: Number,
      },
    });

    if (options && options.treeAttribute) {
      global.logger.info(`Adding tree attribute, ${options.treeAttribute}`);
      this.treeAttribute = options.treeAttribute;
      this.add({
        [this.treeAttribute]: {
          min: 1,
          type: String,
        },
      });
    }

    this.loadClass(HooksMethods);
    this.loadClass(DocumentMethods);
    this.loadClass(NodeCollection);
    // this.static(Statics);

    this.hooks();
  }
  private hooks() {
    // this.pre('save', async function(this: NodeDocument, next: HookNextFunction, err: any) {
    //  await this.preSave(next, err);
    // next(); not needed in async ?
    // });

    this.pre('save', async function(this: NodeDocument, next: HookNextFunction, err: any) {
      /**
       * Update or create
       */
      this.creating = this.isNew ? true : false;
      if (this.creating) {
        await this.beforeCreate(next, err);
      } else {
        await this.beforeUpdate(next, err);
      }
    });

    this.pre('remove', async function() {
      // console.log('pre remove', this);
    });
    this.post('update', async function(this: NodeDocument, error: MongoError, doc: NodeDocument, next: HookNextFunction) {
      // console.log('post update', this);
    });
    this.post('save', this.postSave);
    this.post('remove', this.postRemove);
  }
  private async afterCreate(this: NodeDocument, next: HookNextFunction) {

    switch (this.action) {
      case this.schema.actions.makeRoot:
        if (this.schema.treeAttribute) {
          await this.constructor.updateMany(this.constructor.where('_id').equals(this._id), {[this.schema.treeAttribute]: this._id});
        }
        break;
    }
  }
  private async afterUpdate(this: NodeDocument, next: HookNextFunction) {

   if (this.nodeModel) {
      switch (this.action) {
        case this.schema.actions.appendTo:
          const rightValue = this.nodeModel[this.schema.attributes.rgt];
          await this.moveNode(rightValue, 1);

          let docthis1 = await MenuItem.findById(this._id);
          let docnode1 = await MenuItem.findById(this.nodeModel._id);

          console.log('after update this :', docthis1);
          console.log('after update this action :', this.action);
          console.log('after update nodeModel:', docnode1);
          break;
      }
   }
  }

  /**
   * After save nodes logic
   * @param  this [description]
   * @param  doc  [description]
   * @param  next [description]
   * @return      [description]
   */
  private async postSave(this: NodeDocument, doc: NodeDocument, next: HookNextFunction) {
    // console.log('post save this', this);
    // console.log('post save this', this);
    // console.log('post save this.treeAttribute', this.treeAttribute);
    // console.log('post save new', this.isNew);

    let docthis = await MenuItem.findById(this._id);
    let docnode = await MenuItem.findById(this.nodeModel._id);

    console.log('post save this :', docthis);
    console.log('post save this action :', this.action);
    console.log('post save nodeModel:', docnode);

    if (this.creating) {
      await this.afterCreate(next);
    } else {
      await this.afterUpdate(next);
    }
    this.action = null;
    this.nodeModel = null;
  }
  private async postRemove(this: NodeDocument, error: MongoError, doc: NodeDocument, next: HookNextFunction) {
    //console.log('post remove', this);
    //console.log('isLeaf', this.isLeaf());

    const leftValue = this[this.schema.attributes.lft];
    const rightValue = this[this.schema.attributes.rgt];

    if (this.isLeaf() === true || this.action === this.schema.actions.removeAll) {
      await this.shiftLeftRightAttribute(rightValue + 1, leftValue - rightValue - 1);
    } else {
      const condition = {
        [this.schema.attributes.lft]: {
          $gte: leftValue,
        },
        [this.schema.attributes.rgt]: {
          $lte: rightValue,
        },
      };
      await this.constructor.updateMany(condition, {
        $inc: {
          [this.schema.attributes.lft]: -1,
          [this.schema.attributes.rgt]: -1,
          [this.schema.attributes.depth]: -1,
        },
      });
      await this.shiftLeftRightAttribute(rightValue + 1, -2);
    }
    this.action = null;
    this.nodeModel = null;
    // this.node = null; // sure is not needed
  }
}
