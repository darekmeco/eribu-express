import _filter from 'lodash/fp/filter';
import _flow from 'lodash/fp/flow';
import _head from 'lodash/fp/head';
import _isEmpty from 'lodash/fp/isEmpty';
import _remove from 'lodash/remove';
import { NodeDocument, NodeObject } from '@eribu/nestedset-mongoose';

export class NodeCollection {

  /**
   * Return parent from collection
   * @param  {Model}  model      model with nestedset-mongoose scheme
   * @param  {Collection}  collection model with nestedset-mongoose scheme
   * @return {Model}
   */
  public static getNodeObjectCollectionParent(nodeObject: NodeObject, collection: any): NodeObject | undefined {
    const parent = _flow(
      _filter((o: NodeObject) => {
        return o.lft < nodeObject.lft && o.rgt > nodeObject.rgt && o.depth >= nodeObject.depth - 1;
      }),
      _head,
    )(collection);
    return parent;
  }
  /**
   * Create nested array of collection array
   * @param  collection [description]
   * @return            [description]
   */
  public static getNestedArrayObjectCollection(collection: NodeDocument[]): any {

    const tree: NodeObject[] = [];
    collection.forEach((doc: NodeDocument) => {
      const node: NodeObject = doc.toObject();
      node.title = `${node.name}`; // ${node.name} (${model.isLeaf()}) (lft: ${node.lft} rgt: ${node.rgt}) (${node._id});
      node.children = [];
      node.key = node._id;
      node.value = node._id;
      node.state = {        
        expanded: true,
      };
      tree.push(node);
    });
    const childKeys: number[] = [];
    tree.forEach((node: NodeObject) => {
      const parent: NodeObject | undefined = this.getNodeObjectCollectionParent(node, tree);
      console.log('node:', node);
      console.log('parent:', parent);
      if (parent) {
        parent.children.push(node);
        childKeys.push(node._id);
      }
    });
    _remove(tree, (o: NodeObject) => {
      console.log('o.id', childKeys.includes(o._id));
      return childKeys.includes(o._id);
    });
    return tree;
  }

  /**
   * Check if model has parents in collection
   * @param  {Model}  model      model with nestedset-mongoose scheme
   * @param  {Collection}  collection model with nestedset-mongoose scheme
   * @return {Boolean}
   */
  public hasCollectionParents(this: NodeDocument, collection: any) {
    const parents = _filter(collection, (o: NodeDocument) => {
      return o.lft < this.lft && o.rgt > this.rgt;
    });
    return !_isEmpty(parents);
  }
  /**
   * Return parent from collction
   * @param  {Model}  model      model with nestedset-mongoose scheme
   * @param  {Collection}  collection model with nestedset-mongoose scheme
   * @return {Model}
   */
  public getCollectionParent(this: NodeDocument, collection: any): NodeDocument | undefined {
    const parent = _flow(
      _filter((o: NodeDocument) => {
        return o.lft < this.lft && o.rgt > this.rgt && o.depth >= this.depth - 1;
      }),
      _head,
    )(collection);
    return parent;
  }

}
