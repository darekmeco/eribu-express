import { Schema } from 'mongoose';
import _includes from 'lodash/includes';
import _remove from 'lodash/remove';
import _isEmpty from 'lodash/isEmpty';
// import collections from './collections';
import { NodeDocument } from '@eribu/nestedset-mongoose';

export const Statics = {
  async roots() {
    const roots: NodeDocument[] = await this.find({
      [this.schema.attributes.lft]: 1,
    }); // check order on yii nested?
    return roots;
  },
  async root(): Promise<NodeDocument> {
    const root = await this.findOne({
      [this.schema.attributes.lft]: 1,
    });
    return root;
  },
  /**
   * Checks if roots exists
   * @return {Promise<boolean>}
   */
  async hasRoots(): Promise<boolean> {
    return !_isEmpty(await this.roots());
  },

  // async nestedArray() {
  //   const models = await this.find().sort({
  //     lft: 'asc',
  //   });
  //   const tree: any[] = [];
  //   models.forEach((model: NodeDocument) => {
  //
  //     const node: any = model.toObject();
  //     node.title = `${node.name}`;
  //     // ${node.name} (${model.isLeaf()}) (lft: ${node.lft} rgt: ${node.rgt}) (${node._id});
  //     node.children = [];
  //     node.key = node._id;
  //     node.value = node._id;
  //     node.state = {
  //       expanded: true,
  //     };
  //     tree.push(node);
  //   });
  //
  //   const childKeys: number[] = [];
  //
  //   tree.forEach((node: NodeDocument) => {
  //     const parent: NodeDocument | undefined = collections.getParent(node, tree);
  //     if (parent) {
  //       parent.children.push(node);
  //       childKeys.push(node._id);
  //     }
  //   });
  //
  //   _remove(tree, (o: NodeDocument) => {
  //     console.log('o.id', _includes(childKeys, o._id));
  //     return _includes(childKeys, o._id);
  //   });
  //
  //   return tree;
  // },

};
