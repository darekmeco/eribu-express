import boom from '@hapi/boom';
import _isEmpty from 'lodash/isEmpty';
import { Arr } from '@eribu/helpers';
import { HookNextFunction } from 'mongoose';
import { NodeDocument } from '@eribu/nestedset-mongoose';

export const Methods = {
  isLeaf(): boolean {
    return (this[this.schema.attributes.rgt] - this[this.schema.attributes.lft] === 1);
  },
  // /**
  //  * Make root from node
  //  * @param  this [description]
  //  * @return      [description]
  //  */
  // async makeRoot(this: NodeDocument) {
  //   if (Arr.notEmpty(await this.constructor.roots()) && !this.schema.treeAttribute) {
  //     throw boom.badRequest('Can not create more than one root when "treeAttribute" is false.');
  //   }
  //   this.action = this.schema.actions.makeRoot;
  //   console.log('tree attribute:', this.schema.treeAttribute);
  //   console.log('tree roots:', await this.constructor.roots());
  //   console.log('makeRoot:', this);
  // },

  // async saveRoot(this: NodeDocument): Promise<NodeDocument> {
  //   await this.makeRoot();
  //   return await this.save();
  // },
  /**
   * Appends Child node to Current node
   * @param  {Model} node Child Model mongoose
   */
  async append(node: NodeDocument): Promise<NodeDocument> {
    node.action = node.schema.actions.append;
    node.nodeModel = this;
    return await node.save();
  },
  /**
   * Appends Current node to Parent node
   * @param  {Model} node Parent Model mongoose
   */
  async appendTo(node: NodeDocument): Promise<NodeDocument> {
    this.action = node.schema.actions.appendTo;
    this.nodeModel = node;
    return await this.save();
  },
  /**
   * Insert this after node
   * @param  node [description]
   * @return      [description]
   */
  async insertAfter(node: NodeDocument): Promise<NodeDocument> {
    if (node.isRoot() === true) {
      throw boom.badRequest('Can\'t add more than one root.');
    } else {
      this.action = node.schema.actions.insertAfter;
      this.nodeModel = node;
      return await this.save();
    }
  },
  /**
   * Insert this after node
   * @param  node [description]
   * @return      [description]
   */
  async insertBefore(node: NodeDocument): Promise<NodeDocument> {
    if (node.isRoot() === true) {
      throw boom.badRequest('Can\t add more than one root.');
    } else {
      this.action = node.schema.actions.insertBefore;
      this.nodeModel = node;
      return await this.save();
    }
  },
  /**
   * Prepends Child node to Current node
   * @param  {Model} node Model mongoose
   */
  async prepend(node: NodeDocument): Promise<NodeDocument> {
    node.action = node.schema.actions.prepend;
    node.nodeModel = this;
    return await node.save();
  },
  async beforeInsertRootNode() {
    console.log('beforeInsertRootNode');

    this[this.schema.attributes.lft] = 1;
    this[this.schema.attributes.rgt] = 2;
    this[this.schema.attributes.depth] = 0;
    this.creating = true;
    console.log('beforeInsertRootNode next');
    // return await this.save();
    // return next();
  },
  async beforeInsertNode(this: NodeDocument, value: number, depth: number) {
    if (this.nodeModel) {
      console.log('beforeInsertNode', value, depth, this.nodeModel[this.schema.attributes.depth]);

      this[this.schema.attributes.lft] = value;
      this[this.schema.attributes.rgt] = value + 1;
      this[this.schema.attributes.depth] = this.nodeModel[this.schema.attributes.depth] + depth;

      await this.constructor.updateMany(this.constructor.where(this.schema.attributes.lft).gte(value), {
        $inc: {
          lft: 2,
        },
      });
      await this.constructor.updateMany(this.constructor.where(this.schema.attributes.rgt).gte(value), {
        $inc: {
          rgt: 2,
        },
      });

      // await this.constructor.where(this.schema.attributes.lft).gte(value).updateMany({
      //   $inc: {
      //     lft: 2,
      //   },
      // }).exec(); // is needed ?

      // await this.constructor.where({
      //   rgt: {
      //     $gte: value,
      //   },
      // }).updateMany({
      //   $inc: {
      //     rgt: 2,
      //   },
      // }).exec(); // is needed ?
    }
    // this.action = null;
    // this.nodeModel = null;

    console.log('saving node');
    console.log('saved node');
  },
  async removeNode(this: NodeDocument, all: boolean = false) {
    this.action = all ? this.schema.actions.removeAll : this.schema.actions.remove;
    if (this.action === this.schema.actions.removeAll) {
      const leftValue = this[this.schema.attributes.lft];
      const rightValue = this[this.schema.attributes.rgt];
      const condition = {
        [this.schema.attributes.lft]: {
          $gte: leftValue,
        },
        [this.schema.attributes.rgt]: {
          $lte: rightValue,
        },
      };
      await this.constructor.deleteMany(condition);
    } else {
      await this.remove();
    }
  },
  async shiftLeftRightAttribute(value: number, delta: number) {

    for (const attr of [this.schema.attributes.lft, this.schema.attributes.rgt]) {
      const condition = {
        [attr]: {
          $gte: value,
        },
      };
      await this.constructor.updateMany(condition, {
        $inc: {
          [attr]: delta,
        },
      });
    }
  },
  isRoot(): boolean {
    return this[this.schema.attributes.lft] === 1;
  },
};
