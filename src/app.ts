import cookieParser from 'cookie-parser';
import cors from 'cors';
import express, { NextFunction, Request, Response } from 'express';
import fileUpload from 'express-fileupload';
import mongoose from 'mongoose';
import { logger } from '@eribu/logger';
import path from 'path';
import { FileSystem } from '@eribu/filesystem';
import { EribuModules } from '@eribu/modules';
import * as routes from './routes';
import { AuthProvider } from './modules/authentication/providers/AuthProvider';
import boom from '@hapi/boom';
import morgan from 'morgan';
import { EribuRouter } from '@eribu/router';

global.baseDir = __dirname;
global.assetsDir = path.join(__dirname, '/public/assets');
global.logger = logger;

class App {
  public app: express.Application;
  constructor() {
    this.app = express();
    this.registerProcessErrorHandler();
    this.connectToTheDatabase();
    this.registerAuthorization();
    this.registerMiddlewares();
    this.registerRoutes();
    this.registerModules();
    this.checkLocalStorages();
  }
  private async connectToTheDatabase() {
    const {
      MONGO_USER,
      MONGO_PASSWORD,
      MONGO_HOST,
      MONGO_PORT,
      MONGO_DB,
    } = process.env;
    const credentials: string = MONGO_USER && MONGO_PASSWORD ? `${MONGO_USER}:${MONGO_PASSWORD}@` : '';
    console.log(`mongodb://${credentials}${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB}`);
    await mongoose.connect(`mongodb://${credentials}${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB}`, {
      useNewUrlParser: true,
      useCreateIndex: true,
    }).catch((error) => {
      throw boom.notFound(error);
    });
  }
  /**
   * [registerMiddlewares description]
   * @return [void]
   */
  private registerMiddlewares(): void {
    this.app.use(morgan('combined'));
    // this.app.use(morgan('combined', morganOptions));

    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(cookieParser());
    this.app.use(express.static(path.join(__dirname, 'public')));
    this.app.use(fileUpload());
    this.app.use(cors());
    // this.app.use(expressValidator());
  }
  private registerRoutes() {
    routes.register(this.app);
    global.router = new EribuRouter();
  }
  private async registerModules() {
    const modules = new EribuModules(this.app);
    global.logger.info('Registering Modules');
    await modules.init();
    /**
     * last middlewares
     */
    this.registerErrorHandler();
  }
  /**
   * Register errohandlers for node process
   * @return [void]
   */
  private registerProcessErrorHandler(): void {
    global.logger.info('Registering Process Error Handler');
    process
      .on('unhandledRejection', (reason: boom, p) => {
        global.logger.info('Got unhandled Rejection!');
        if (reason.isBoom) {
          // console.log(reason.output.payload.message);
          logger.debug(`Unhandled Rejection: ${reason.output.payload.message}`);
        } else {
          logger.debug(`Unhandled Rejection (no-boom): ${p}`);
          console.log('unhandledRejection not boom:', p);
        }
      })
      .on('uncaughtException', (err) => {
        global.logger.info('Got unhandled Exception!');
        // console.log('uncaughtException', err);
      });
  }
  /**
   * Error handler has to be registered as last middleware
   * @return [description]
   */
  private registerErrorHandler() {
    this.app.use((err: any, req: Request, res: Response, next: NextFunction) => {
      // global.logger.info('ErrorHandler: ', err);
      // console.log('ErrorLog: ', err);
      // console.log(err);
      const error = err.output.payload;
      error.data = err.data;
      return res.status(err.output.statusCode).json(error);
    });
    global.logger.info('Registering Error Handler');
  }

  private registerAuthorization() {
    const auth = new AuthProvider();
  }
  /**
   * checks if local directories exists
   * @return {boolean} [true if exists]
   */
  private checkLocalStorages(): boolean {
    FileSystem.checkDirectorySync(global.assetsDir);
    return true;
  }
}

export default new App().app;
