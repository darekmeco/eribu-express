#!/usr/bin/env node
import debugModule from 'debug';
import 'dotenv/config';
import app from '../app';
const debug = debugModule('extpresstest:server');
import http from 'http';
import { AddressInfo } from 'net';

const {
  PORT,
} = process.env;

app.set('port', normalizePort(PORT || '3000'));
const server: http.Server = http.createServer(app);
server.listen(app.get('port'));
server.on('error', onError);
server.on('listening', onListening);

function normalizePort(val: string): number {
  const port: number = parseInt(val, 10);
  if (port >= 0) {
    // port number
    return port;
  }
  return 0;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error: any) {

  if (error.syscall !== 'listen') {
    throw error;
  }
  const port: number = app.get('port');
  const bind: string = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr: string | AddressInfo | null = server.address();

  if (addr) {
    const bind = (addr != null && typeof addr === 'string')
      ? 'pipe ' + addr
      : 'port ' + addr.port;
    debug('Listening on ' + bind);
  }
}
