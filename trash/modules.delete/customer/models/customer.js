'use strict'

const mongoose = require('mongoose')
const validator = require('validator');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: [true, 'Wymagane']
  },
  email: {
    type: String,
    required: [true, 'Wymagane'],
    validate: {
      validator: validator.isEmail,
      message: 'Invalid email.',
    },

  },
  firstname: {
    type: String,
    required: [true, 'Wymagane'],

  },
  lastname: {
    type: String,
    required: [true, 'Wymagane']
  },
  description: String

});

module.exports = mongoose.model('Customer', schema);
