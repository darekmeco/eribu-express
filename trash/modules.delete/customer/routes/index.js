var express = require('express');
const CustomerController = require('../controllers/customerController');
var router = express.Router();

const controller = new CustomerController();
router.get('/', controller.index);
router.post('/store', controller.store);

module.exports = router;
