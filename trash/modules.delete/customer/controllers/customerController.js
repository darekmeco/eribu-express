"use strict";
const Customer = require('../models/customer');

class CustomerController {
  async index(req, res) {
    const customers = await Customer.find();
    res.json(customers);
  }
  async store(req, res) {

    const model = new Customer(req.body);
    
    const error = model.validateSync();

    if (error === undefined) {
      await model.save();
    }

    res.json({
      status: 'success',
      test: 1,
      error: error,
      params: req.params,
      body: req.body,
      query: req.query
    });
  }
}

module.exports = CustomerController;
