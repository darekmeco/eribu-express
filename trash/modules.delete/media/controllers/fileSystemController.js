"use strict"
const File = require(__basedir + '/modules/media/models/file');
const fileSystem = require(__basedir + '/libs/filesystem');
var _chunk = require("lodash/chunk");
var _size = require("lodash/size");
var _get = require("lodash/get");

class FileSystemController {

  index(req, res) {
    var startdir = './';
    return res.json(fileSystem.readFoldersTree(startdir));
  }

  files(req, res) {

    console.log(req.body);

    const page = req.body.current && req.body.current > 0 ? req.body.current - 1 : 1;

    if (typeof(req.body.current) === "number") {

      const allFiles = fileSystem.readFilesWithPath(req.body.path);
      const size = _size(allFiles);
      const perPage = typeof(req.body.perPage) === "number" ? req.body.perPage : 10;

      const filesPage = _get(
          _chunk(
            fileSystem.readFilesWithPath(req.body.path), perPage
          ), page, []);

        return res.json({
          files: filesPage,
          size: size
        });
    }
    return res.sendStatus(404);
  }
  all(req, res) {
    const dir = './';

    const read = (dir) => {
      return fs.readdirSync(dir).reduce((files, file) => {
        console.log(files);
        const dirent = fs.statSync(path.join(dir, file));
        console.log(dirent.isDirectory());
        if (dirent.isDirectory()) {
          console.log(path.join(dir, file));
          return files.concat(read(path.join(dir, file)))
        } else {
          console.log(path.join(dir, file));
          return files.concat(path.join(dir, file));
        }
      }, []);
    }

    res.json({
      files: read(dir)
    });

  }
  store(req, res) {
    res.json({});
  }
  delete(req, res) {
    const filename = req.body.filename;
    const searchpath = req.body.searchpath;
    fileSystem.deleteFileWithPath(searchpath, filename)
    res.json({status: 'success'});
  }
}

module.exports = FileSystemController;
