"use strict";
const File = require('../models/file');

class FileController {
  async index(req, res) {
    const files = {
      a: 1
    };
    res.json(files);
  }
  async store(req, res) {
    res.json({});
  }
  upload(req, res) {

    const file = req.files.file;
    const path = req.body.path;
    const dir = __assetsdir + '/' + path + '/' + file.name;

    console.log(dir);
    console.log(req.files);

    file.mv(dir, function(err) {
      if (err) {
        return res.status(500).send(err);
      }

      return res.send('File uploaded to ' + dir);
    });



  }
}

module.exports = FileController;
