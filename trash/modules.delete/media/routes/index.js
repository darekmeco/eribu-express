var express = require('express');
const FileController = require('../controllers/fileController');
const FileSystemController = require('../controllers/fileSystemController');

var router = express.Router();

const fileController = new FileController();
const fileSystemController = new FileSystemController();

router.get('/', fileController.index);
router.post('/store', fileController.store);

router.post('/upload', fileController.upload);
router.put('/upload', fileController.upload);

router.get('/filesystem/index', fileSystemController.index);
router.post('/filesystem/files', fileSystemController.files);
router.delete('/filesystem/files', fileSystemController.delete);

module.exports = router;
