'use strict'

const mongoose = require('mongoose')
const validator = require('validator');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: [true, 'Wymagane']
  },
  filename: {
    type: String,
    required: [true, 'Wymagane'],
  },
  path: {
    type: String,
    required: [true, 'Wymagane']
  },
  description: String
});

module.exports = mongoose.model('File', schema);
