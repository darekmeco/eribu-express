'use strict'

const mongoose = require('mongoose')
const validator = require('validator');
const slugify = require('slugify-mongoose')


const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: [true, 'Wymagane']
  },
  slug: {
    type: String,
    slug: 'name',
    unique: true
  },
  description: {
    type: String,
    required: [true, 'Wymagane'],
  },
  price: {
    type: Number,
    required: [true, 'Wymagane'],
  },
  thumb: {
    filename: {
      type: String
    },
    searchpath: {
      type: String
    }
  },
  gallery: {
    type: Array,
  },

}, {
  timestamps: true
});
schema.plugin(slugify)
module.exports = mongoose.model('Product', schema);
