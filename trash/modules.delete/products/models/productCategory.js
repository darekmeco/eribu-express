'use strict'

const mongoose = require('mongoose')
const slugify = require('slugify-mongoose')
const NestMongoose = require(__basedir + '/libs/nestedset-mongoose');

const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: [true, 'Wymagane']
  },
  slug: {
    type: String,
    slug: 'name',
    unique: true
  },
  thumb: {
    filename: {
      type: String
    },
    searchpath: {
      type: String
    }
  },
}, {
  timestamps: true
});
schema.plugin(slugify)
schema.plugin(NestMongoose);
module.exports = mongoose.model('ProductCategory', schema);
