"use strict";
const Product = require('../models/product');

class ProductController {

  async index(req, res) {
    const models = await Product.find().sort({createdAt: 'desc'});
    res.json(models);
  }
  
  async store(req, res) {
    const model = new Product(req.body);
    model.validate(async (error) => {
      console.log('error', error);
      if (error == null) {
        await model.save();
      }
      return res.json({
        model: model,
        status: 'success',
        test: 1,
        error: error,
        params: req.params,
        body: req.body,
        query: req.query
      });
    });
  }
  async update(req, res) {

    const model = await Product.findById(req.body._id);

    model.validate(async (error) => {

      console.log('error', error);
      if (error == null) {
        console.log('update model', model);
        console.log('up m req.body', req.body);
        await model.update({
          $set: req.body
        });
      }

      return res.json({
        model: model,
        status: 'success',
        test: 1,
        error: error,
        params: req.params,
        body: req.body,
        query: req.query
      });
    });
  }
  async edit(req, res) {
    const model = await Product.findById(req.query.id);
    return res.json({
      status: 'success',
      model: model,
    });
  }
}

module.exports = ProductController;
