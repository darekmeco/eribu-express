"use strict";
const ProductCategory = require('../models/productCategory');
const _get = require("lodash/get");
const _keyBy = require("lodash/keyBy");
const boom = require('boom');

const collections = require(__basedir + '/libs/nestedset-mongoose/collections')

class ProductCategoryController {

  /**
   * Delete node with its children
   * @param  {Object}   req  [description]
   * @param  {Object}   res  [description]
   * @param  {Function} next [description]
   * @return {Promise}       [description]
   */
  async delete(req, res, next) {
    const model = await ProductCategory.findById(req.body.id); //short if check
    try {
      await model.removeNode(req.body.all);
    } catch (err) {
      return next(err);
    }
    return res.json(await ProductCategory.nestedArray());
  }

  async move(req, res, next) {
    console.log(req.body);
    const dropPosition = req.body.dropPosition;
    try {

      const dropNode = await ProductCategory.findById(req.body.dropKey);
      const dragNode = await ProductCategory.findById(req.body.dragKey);

      switch (dropPosition) {
        case 0:
          await dragNode.appendTo(dropNode);
          break;
      }

    } catch (err) {
      console.log(err);
      return next(boom.badRequest(err));
    }
    return res.json(await ProductCategory.nestedArray());
  }

  async init(req, res, next) {
    //const models = await Product.find().sort({createdAt: 'desc'});

    console.log('hasRoots: ', await ProductCategory.hasRoots());
    let root = null;
    if (await ProductCategory.hasRoots() === true) {
      root = await ProductCategory.root();
    } else {
      root = new ProductCategory({
        name: 'root'
      });
      const err = await root.makeRoot(next);
      if (_get(err, 'isBoom', false)) {
        return next(err);
      }
    }

    //
    console.log('maked root', root);
    //
    const child1 = new ProductCategory({
      name: 'child1'
    });
    await root.append(child1);
    const child2 = new ProductCategory({
      name: 'child2'
    });
    await root.append(child2);
    const child3 = new ProductCategory({
      name: 'child3'
    });
    await child1.append(child3);
    const child4 = new ProductCategory({
      name: 'child4'
    });
    await child3.prepend(child4);

    //const roots = await ProductCategory.roots(next);
    //console.log('roots:', roots);
    //if (roots) {
    //  return res.json(req.query);
    //}

    res.json(req.query);
  }
  async tree(req, res, next) {
    return res.json(await ProductCategory.nestedArray());
  }
}
module.exports = ProductCategoryController;
