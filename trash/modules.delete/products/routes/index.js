const express = require('express');
const ProductController = require('../controllers/productController');
const ProductCategoryController = require('../controllers/productCategoryController');
const router = express.Router();

const controller = new ProductController();
const categoryController = new ProductCategoryController();
router.get('/index', controller.index);
router.post('/store', controller.store);
router.get('/edit', controller.edit);
router.post('/update', controller.update);
router.get('/category/init', categoryController.init);
router.get('/category/tree', categoryController.tree);
router.delete('/category/delete', categoryController.delete);
router.post('/category/move', categoryController.move);

module.exports = router;
