import _filter from 'lodash/fp/filter';
import _flow from 'lodash/fp/flow';
import _head from 'lodash/fp/head';
import _isEmpty from 'lodash/fp/isEmpty';

import mongoose from 'mongoose';
import { NodeDocument } from './interfaces/NodeDocument';

export default {
  /**
   * Check if model has parents in collection
   * @param  {Model}  model      model with nestedset-mongoose scheme
   * @param  {Collection}  collection model with nestedset-mongoose scheme
   * @return {Boolean}
   */
  hasParents(model: NodeDocument, collection: any) {

    const parents = _filter(collection, (o: NodeDocument) => {
      return o.lft < model.lft && o.rgt > model.rgt;
    });
    return !_isEmpty(parents);
  },
  /**
   * Return parent from collction
   * @param  {Model}  model      model with nestedset-mongoose scheme
   * @param  {Collection}  collection model with nestedset-mongoose scheme
   * @return {Model}
   */
  getParent(model: NodeDocument, collection: any): NodeDocument {
    const parent = _flow(
      _filter((o: NodeDocument) => {
        return o.lft < model.lft && o.rgt > model.rgt && o.depth >= model.depth - 1;
      }),
      _head
    )(collection);

    return parent;
  }
}
