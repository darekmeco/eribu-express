import _includes from 'lodash/includes';
import _isEmpty from 'lodash/isEmpty';
import _remove from 'lodash/remove';
import collections from './collections';
import {NodeDocument} from './interfaces/NodeDocument';
export default {
  /**
   * Gets the root nodes.
   * @return {Promise}
   */
  async roots() {
    const roots = await this.find({
      [this.schema.attributes.lft]: 1
    }); //check order on yii nested?
    return roots;
  },
  /**
   * Checks if roots exists
   * @return {Boolean}
   */
  async hasRoots() {
    return !_isEmpty(await this.roots());
  },
  async root() {
    const root = await this.findOne({
      [this.schema.attributes.lft]: 1
    });
    return root;
  },
  async nestedArray() {
    const models = await this.find().sort({
      lft: 'asc'
    });

    let tree = [];

    for (let model of models) {
      let node = model.toObject();
      node.title = `${node.name} (${model.isLeaf()}) (lft: ${node.lft} rgt: ${node.rgt}) (${node._id})`;
      node.children = [];
      node.key = node._id;
      node.state = {
        expanded: true
      };
      tree.push(node);
    }

    const childKeys: number[] = [];
    for (const node of tree) {
      const parent:NodeDocument = collections.getParent(node, tree);
      if (parent) {
        parent.children.push(node);
        childKeys.push(node._id);
      }
    }

    _remove(tree, (o) => {
      console.log('o.id', _includes(childKeys, o._id));
      return _includes(childKeys, o._id);
    });

    return tree;
  },

}
