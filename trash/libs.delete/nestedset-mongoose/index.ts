import mongoose, { HookNextFunction } from 'mongoose';
import { NodeDocument } from './interfaces/NodeDocument';
import { NodeSchema } from './interfaces/NodeSchema';
import methods from './methods';
import statics from './statics';
import { MongoError } from 'mongodb';

export const nestedSetPlugin = (schema: NodeSchema, options: mongoose.SchemaOptions) => {

  schema.attributes = Object.freeze({
    depth: 'depth',
    lft: 'lft',
    rgt: 'rgt',
  });

  schema.actions = Object.freeze({
    append: 'append-node',
    appendTo: 'appendto-node',
    prepend: 'prepend-node',
    prependTo: 'prepend-node',
    remove: 'remove',
    removeAll: 'remove-all',
  });

  schema.action = null;

  schema.add({
    [schema.attributes.lft]: {
      type: Number,
      min: 1
    },
    [schema.attributes.rgt]: {
      type: Number,
      min: 1
    },
    [schema.attributes.depth]: {
      type: Number,
      min: 0
    },
  });

  schema.method(methods);
  schema.static(statics);

  // schema.virtual('children').
  // get(function() { return this.children; }).
  // set(function(v) {
  //   this.name.first = v.substr(0, v.indexOf(' '));
  // });

  schema.pre('remove', async function() {
    console.log('pre remove', this);
    // await Promise.resolve();
    // throw boom.badRequest('bad request');
  });

  // post.update

  schema.post('update', async function(error: MongoError, doc: NodeDocument, next: HookNextFunction) {
    console.log('post update', this);
  });

  /**
   * Action after node was saved
   * @param  {Function} next [description]
   * @param  {[type]}   err  [description]
   * @return {[type]}        [description]
   */
  schema.post('save', async function(error: MongoError, doc: NodeDocument, next: HookNextFunction) {
    console.log('post save', this);
    console.log('post save new', this.isNew);

    /**
     * Only for update
     * @param  {[type]} this [description]
     */
    if (this.isNew === false) {

      const rightValue = this.nodeModel[this.schema.attributes.rgt];

      switch (this.action) {
        case this.schema.actions.appendTo:
          await this.moveNode(rightValue, 1);
          break;
      }
    }
    this.action = null;
    this.nodeModel = null;
  });

  /**
   * Action after remove node
   * @param  {Function} next [description]
   * @param  {Error}   err  [description]
   * @return {Promise}        [description]
   */
  schema.post('remove', async function(error: MongoError, doc: NodeDocument, next: HookNextFunction) {

    console.log('post remove', this);
    console.log('isLeaf', this.isLeaf());

    const leftValue = this[this.schema.attributes.lft];
    const rightValue = this[this.schema.attributes.rgt];

    if (this.isLeaf() == true || this.action == this.schema.actions.removeAll) {
      await this.shiftLeftRightAttribute(rightValue + 1, leftValue - rightValue - 1);
    } else {
      const condition = {
        [this.schema.attributes.lft]: {
          $gte: leftValue
        },
        [this.schema.attributes.rgt]: {
          $lte: rightValue
        }
      };
      await this.constructor.updateMany(condition, {
        $inc: {
          [this.schema.attributes.lft]: -1,
          [this.schema.attributes.rgt]: -1,
          [this.schema.attributes.depth]: -1,
        }
      });
      await this.shiftLeftRightAttribute(rightValue + 1, -2);
    }
    this.action = null;
    this.nodeModel = null;
    // this.node = null; // sure is not needed
  });

  schema.pre('save', async function(this: NodeDocument, next: HookNextFunction, err: any) {
    console.log('pre save this :', this);
    console.log('pre save this action :', this.action);
    console.log('pre save:', this.nodeModel);
    switch (this.action) {
      case this.schema.actions.append:
        await this.beforeInsertNode(this.nodeModel.rgt, 1);
        break;
      case this.schema.actions.appendTo:
        await this.beforeInsertNode(this.nodeModel.rgt, 1);
        break;
      case this.schema.actions.prepend:
        await this.beforeInsertNode(this.nodeModel.lft + 1, 1);
        break;
    }

    // next(); not needed in async ?

  });


};
