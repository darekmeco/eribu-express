import { Schema } from 'mongoose';

interface Actions {
  append: string;
  appendTo: string;
  prepend: string;
  prependTo: string;
  remove: string;
  removeAll: string;
}

interface Attributes {
    depth: string;
    lft: string;
    rgt: string;
}

export interface NodeSchema extends Schema {
  attributes: Attributes;
  actions: Actions;
  action: string;
}
