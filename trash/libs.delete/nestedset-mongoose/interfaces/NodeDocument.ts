import { Document } from 'mongoose';
import { NodeSchema } from './NodeSchema';

export interface NodeDocument extends Document {
   children: NodeDocument[];
   schema: NodeSchema;
}
