import boom from 'boom';
import isEmpty from 'lodash/isEmpty';
import mongoose from 'mongoose';
import MethodsInterface from "./interfaces/";

class Methods extends MethodsInterface {

  public makeRoot() {
    return true;
  }

  public async removeNode(all:boolean = false) {

    this.action = all ? this.schema.actions.removeAll : this.schema.actions.remove;
    if (this.action == this.schema.actions.removeAll) {
      const leftValue = this[this.schema.attributes.lft];
      const rightValue = this[this.schema.attributes.rgt];
      const condition = {
        [this.schema.attributes.lft]: {
          $gte: leftValue
        },
        [this.schema.attributes.rgt]: {
          $lte: rightValue
        }
      };
      await this.constructor.deleteMany(condition);
    } else {
      await this.remove();
    }
  }

  public moveNode(value, depth) {
    let leftValue = this[this.schema.attributes.lft];
    let rightValue = this[this.schema.attributes.rgt];
    const depthValue = this[this.schema.attributes.depth];
    const depthAttribute = this.schema.attributes.depth;
    const computedDepth = this.nodeModel[this.schema.attributes.depth] - depthValue + depth;
    const delta = rightValue - leftValue + 1;

    await this.shiftLeftRightAttribute(value, delta);

    if (leftValue >= value) {
      leftValue += delta;
      rightValue += delta;
    }

    const condition = {
      [this.schema.attributes.lft]: {
        $gte: leftValue
      },
      [this.schema.attributes.rgt]: {
        $lte: rightValue
      }
    };
    await this.constructor.updateMany(condition, {
      $inc: {
        [depthAttribute]: computedDepth
      }
    });

    for (let attr of [this.schema.attributes.lft, this.schema.attributes.rgt]) {

      const condition = {
        [attr]: {
          $gte: leftValue
        },
        [attr]: {
          $lte: rightValue
        }
      };

      await this.constructor.updateMany(condition, {
        $inc: {
          [attr]: (value - leftValue)
        }
      });
    };
  }


}

export default Methods;

// module.exports = {
//   makeRoot: async function(next) {
//
//     if (isEmpty(await this.constructor.roots()) === false) {
//       return boom.badRequest('Can not create more than one root when "treeAttribute" is false.');
//     }
//
//     this.lft = 1;
//     this.rgt = 2;
//     this.depth = 0;
//     await this.save();
//     console.log('making root');
//     return this;
//
//   },
//   /**
//    * Appends Child node to Current node
//    * @param  {Model} node Child Model mongoose
//    */
//   async append(node) {
//     node.action = node.schema.actions.append;
//     node.nodeModel = this;
//     await node.save();
//   },
//   /**
//    * Appends Current node to Parent node
//    * @param  {Model} node Parent Model mongoose
//    */
//   appendTo: async function(node) {
//     this.action = node.schema.actions.appendTo;
//     this.nodeModel = node;
//     await this.save();
//   },
//   /**
//    * Prepends Child node to Current node
//    * @param  {Model} node Model mongoose
//    */
//   prepend: async function(node) {
//     node.action = node.schema.actions.prepend;
//     node.nodeModel = this;
//     await node.save();
//   },
//   /**
//    * Prepends Current node to Parent node
//    * @param  {Model} node Model mongoose
//    */
//   prependTo: async function(node) {
//     node.action = node.schema.actions.prependTo;
//     node.nodeModel = node;
//     await this.save();
//   },
//   /**
//    * Remove tree node
//    * @param  {Boolean} [all=false] [description]
//    * @return {Promise}              [description]
//    */
//   removeNode: async function(all = false) {
//
//     this.action = all ? this.schema.actions.removeAll : this.schema.actions.remove;
//     if (this.action == this.schema.actions.removeAll) {
//       const leftValue = this[this.schema.attributes.lft];
//       const rightValue = this[this.schema.attributes.rgt];
//       const condition = {
//         [this.schema.attributes.lft]: {
//           $gte: leftValue
//         },
//         [this.schema.attributes.rgt]: {
//           $lte: rightValue
//         }
//       };
//       await this.constructor.deleteMany(condition);
//     } else {
//       await this.remove();
//     }
//   },
//
//   moveNode: async function(value, depth) {
//
//     let leftValue = this[this.schema.attributes.lft];
//     let rightValue = this[this.schema.attributes.rgt];
//     const depthValue = this[this.schema.attributes.depth];
//     const depthAttribute = this.schema.attributes.depth;
//     const computedDepth = this.nodeModel[this.schema.attributes.depth] - depthValue + depth;
//     const delta = rightValue - leftValue + 1;
//
//     await this.shiftLeftRightAttribute(value, delta);
//
//     if (leftValue >= value) {
//       leftValue += delta;
//       rightValue += delta;
//     }
//
//     const condition = {
//       [this.schema.attributes.lft]: {
//         $gte: leftValue
//       },
//       [this.schema.attributes.rgt]: {
//         $lte: rightValue
//       }
//     };
//     await this.constructor.updateMany(condition, {
//       $inc: {
//         [depthAttribute]: computedDepth
//       }
//     });
//
//     for (let attr of [this.schema.attributes.lft, this.schema.attributes.rgt]) {
//
//       const condition = {
//         [attr]: {
//           $gte: leftValue
//         },
//         [attr]: {
//           $lte: rightValue
//         }
//       };
//
//       await this.constructor.updateMany(condition, {
//         $inc: {
//           [attr]: (value - leftValue)
//         }
//       });
//     };
//   },

//   isLeaf: function() {
//     return (this[this.schema.attributes.rgt] - this[this.schema.attributes.lft] == 1);
//   },
//
//   beforeInsertNode: async function(value, depth) {
//     console.log('beforeInsertNode', value, depth, this.nodeModel.depth);
//
//     this.lft = value;
//     this.rgt = value + 1;
//     this.depth = this.nodeModel.depth + depth;
//
//     await this.constructor.where({
//       lft: {
//         $gte: value
//       }
//     }).updateMany({
//       $inc: {
//         lft: 2
//       }
//     }).exec();
//
//     await this.constructor.where({
//       rgt: {
//         $gte: value
//       }
//     }).updateMany({
//       $inc: {
//         rgt: 2
//       }
//     }).exec();
//
//     //this.action = null;
//     //this.nodeModel = null;
//
//     console.log('saving node');
//     console.log('saved node');
//   },
//   shiftLeftRightAttribute: async function(value, delta) {
//
//     for (let attr of [this.schema.attributes.lft, this.schema.attributes.rgt]) {
//       const condition = {
//         [attr]: {
//           $gte: value
//         }
//       };
//       await this.constructor.updateMany(condition, {
//         $inc: {
//           [attr]: delta
//         }
//       });
//     };
//   }
// }
