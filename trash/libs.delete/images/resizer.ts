import express from 'express';
import fs from 'fs';
import Jimp from 'jimp';

const router = express.Router();

class Resizer {
  public async index(req: express.Request, res: express.Response, next: express.NextFunction) {

    if (req.params.filename) {

      const requestFile = __dirname + '/' + process.env.ASSETS_DIR + '/' + req.params.filename;
      const cacheFile = __dirname + '/' + process.env.ASSETS_DIR + '/cache/' + req.params.filename;

      console.log('req:', requestFile);
      if (fs.existsSync(requestFile) && !fs.existsSync(cacheFile)) {

        console.log('no cache');

        const image = await Jimp.read(requestFile);
        await image.cover(256, 256)
          .quality(80)
          .writeAsync(cacheFile);

      }

      if (fs.existsSync(cacheFile)) {
        return res.sendFile(cacheFile);
      }

    }
    return res.sendStatus(404);
  }
}

const resizer = new Resizer();

router.get('/:filename(*)', resizer.index);

export default router;
