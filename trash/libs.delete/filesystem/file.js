"use strict";
const path = require('path');
const fs = require('fs');

class File {
  constructor(filePath) {

    this.filePath = filePath;
    console.log('filePath: ', this.filePath);
    fs.accessSync(filePath, fs.constants.R_OK);
    this.pathParse();
    this.dirent = fs.statSync(filePath);

  }

  pathParse() {
    const pathParts = path.parse(this.filePath);
    this.dir = pathParts.dir;
    this.root = pathParts.root;
    this.base = pathParts.base;
    this.name = pathParts.name;
    this.ext = pathParts.ext;
  }

  hasThumbnail() {
    const thumb = `${__assetsdir}/cache/${this.name}${this.ext}`;
    try {
      fs.accessSync(thumb, fs.constants.R_OK);
      return true;
    } catch (err) {
      return false;
    }
  }
}

module.exports = File;
