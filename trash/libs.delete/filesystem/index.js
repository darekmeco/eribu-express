"use strict";
const path = require('path');
const fs = require('fs');
const File = require(__basedir + '/libs/filesystem/file');

class FileSystem {

  static readAllTree() {
    var startdir = __assetsdir;
    //var files = [];
    const walkSync = (dir, filelist = []) => {
      const files = fs.readdirSync(dir);
      for (const file of files) {
        const dirFile = path.join(dir, file);
        try {
          const dirent = fs.statSync(dirFile);
        } catch (err) {
          console.log('it does not exist');
          break;
        }
        if (dirent.isDirectory()) {
          console.log('directory', path.join(dir, file));
          const odir = {
            file: dirFile,
            files: []
          }
          odir.files = walkSync(dirFile, dir.files);
          filelist.push(odir);
        } else {
          filelist.push({
            file: dirFile
          });
        }
      }
      return filelist;
    };
    return walkSync(startdir)
  }

  static readFoldersTree() {
    var startdir = __assetsdir;
    //var files = [];
    const walkSync = (dir, filelist = []) => {
      const files = fs.readdirSync(dir);
      for (const file of files) {
        const dirFile = path.join(dir, file);
        try {
          const dirent = fs.statSync(dirFile);
          if (dirent.isDirectory() && file != 'cache') {
            console.log('directory', path.join(dir, file));
            const odir = {
              data: {
                path: dir,
                filepath: dirFile,
                path_relative: path.relative(__assetsdir, dirFile)
              },
              text: file,
              children: []
            }
            odir.children = walkSync(dirFile, dir.files);
            filelist.push(odir);
          }
        } catch (err) {
          console.log(err);
        }
      }
      return filelist;
    };
    return walkSync(startdir)
  }

  static readFilesWithPath(searchpath = __assetsdir) {

    var dir = __assetsdir + '/' + searchpath;

    console.log('dir:', dir);

    const files = fs.readdirSync(dir);

    const fileList = [];
    for (const file of files) {

      const f = new File(path.join(dir, file));

      console.log(f.hasThumbnail());

      if (!f.dirent.isDirectory()) {
        fileList.push({
          filename: file,
          path: dir,
          searchpath: searchpath,
        })
      }
    }
    return fileList;
  };

  static deleteFileWithPath(searchpath, filename) {
    fs.unlinkSync( __assetsdir + '/' + searchpath + '/' + filename);
  }



}

module.exports = FileSystem;
